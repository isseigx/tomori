import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import org.kde.kirigami as Kirigami

ItemDelegate {
    id: delegate

    activeFocusOnTab: GridView.view ? false : true
    checkable: true
    highlighted: focus && GridView.isCurrentItem && GridView.view && GridView.view.keyNavigationEnabled
    hoverEnabled: true

    contentItem: ColumnLayout {
        spacing: Kirigami.Units.mediumSpacing

        Image {
            id: image
            clip: true
            fillMode: Image.PreserveAspectCrop
            source: model.image.toString() === "" ? "qrc:cover.svg" : model.image
            sourceSize.width: Kirigami.Units.gridUnit * 11

            Layout.fillHeight: true
            Layout.fillWidth: true
        }

        Label {
            id: titleText
            elide: Text.ElideRight
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
            text: model.album
            Layout.fillWidth: true
        }

        Label {
            id: subtitleText
            elide: Text.ElideRight
            horizontalAlignment: Text.AlignHCenter
            text: model.artist
            Layout.fillWidth: true
        }
    }

    onClicked: {
        GridView.view.currentIndex = index
        checked = true
    }

    Keys.onEnterPressed: clicked()
    Keys.onReturnPressed: clicked()
}
