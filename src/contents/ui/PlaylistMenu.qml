import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Dialogs 1.3

import io.onigiri.ayase 2.0

Menu {
    MenuItem {
        text: qsTr("Randomize playlist")
        onTriggered: playlistModel.randomize()
    }

    MenuSeparator { }

    MenuItem {
        text: qsTr("Add file...")
        onTriggered: {
            var dialog = fileDialog.createObject(root, {
                selectMultiple: true
            })
            dialog.open()
        }
    }

    MenuItem {
        text: qsTr("Add folder...")
        onTriggered: {
            var dialog = fileDialog.createObject(root, {
                selectFolder: true
            })
            dialog.open()
        }
    }

    MenuItem {
        text: qsTr("Clear playlist")
        onTriggered: {
            player.stop()
            playlistModel.clear()
        }
    }

    MenuSeparator { }

    MenuItem {
        text: qsTr("Open playlist...")
        onTriggered: {
            var dialog = playlistFileDialog.createObject(root, {
                selectExisting: true
            })
            dialog.open()
        }
    }

    MenuItem {
        text: qsTr("Export playlist...")
        onTriggered: {
            var dialog = playlistFileDialog.createObject(root, {
                selectExisting: false
            })
            dialog.open()
        }
           
    }
    
    
    Component {
        id: fileDialog

        FileDialog {
            folder: Utils.currentDir
            nameFilters: [qsTr("Music: (*.flac *.mp3 *.m4a *.ogg *.oga *.opus)")]
            selectMultiple: true
            onAccepted: {
                if (selectFolder) {
                    playlistModel.addFolder(folder)
                }
                else {
                    playlistModel.addFiles(fileUrls)
                }
                Utils.currentDir = folder
                destroy()
            }
            onRejected: destroy()
        }
    }

    Component {
        id: playlistFileDialog

        FileDialog {
            defaultSuffix: "xspf"
            folder: Utils.currentDir
            nameFilters: [qsTr("XSPF playlists (*.xspf)")]
            selectedNameFilter: qsTr("XSPF playlists (*.xspf)")

            onAccepted: {
                if (selectExisting) {
                    playlistModel.open(fileUrl)
                }
                else {
                    playlistModel.saveAs(fileUrl)
                }
                Utils.currentDir = folder
                destroy()
            }

            onRejected: destroy()
        }
    }
}
