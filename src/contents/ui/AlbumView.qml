import QtQuick
import QtQuick.Controls
import QtQuick.Effects
import QtQuick.Layouts

import org.kde.kirigami as Kirigami

import dev.onigiri.ayase

Kirigami.ScrollablePage {
    id: albumView

    property media albumInfo
    property color bgColor: Kirigami.Theme.backgroundColor
    property color fgColor: albumInfo.image ? "white" : Kirigami.Theme.textColor
    
    title: qsTr("Album")

    actions: [
        Kirigami.Action {
            icon.name: "media-playback-start"
            icon.source: "qrc:play.svg"
            text: qsTr("Play")
            onTriggered: {
                player.stop()
                playlistModel.clear()
                playlistModel.addMedia(libraryListModel.mediaList)
                playlistModel.setCurrentIndex(0)
            }
        },
        Kirigami.Action {
            icon.name: "media-playlist-append"
            icon.source: "qrc:queue_music.svg"
            text: qsTr("Enqueue")
            onTriggered: playlistModel.addMedia(libraryListModel.mediaList)
        }
    ]

    SongItemMenu {
        id: menu
    }

    ListView {
        id: mediaView

        header: Pane {
            width: mediaView.width

            RowLayout {
                id: content
                anchors.fill: parent
                anchors.margins: Kirigami.Units.smallSpacing
                spacing: Kirigami.Units.largeSpacing

                Image {
                    asynchronous: true
                    fillMode: Image.PreserveAspectFit
                    height: 125
                    source: albumInfo.image.toString() === "" ? "qrc:cover.svg" : albumInfo.image
                    sourceSize.height: 125
                    width: 125
                    Layout.maximumWidth: 125
                    Layout.maximumHeight: 125
                }

                ColumnLayout {
                    Layout.fillWidth: true

                    Label {
                        text: albumInfo.album
                        elide: Text.ElideRight
                        font.pointSize: Kirigami.Theme.defaultFont.pointSize * 1.35
                        font.weight: Font.DemiBold
                        Layout.fillWidth: true
                    }

                    Label {
                        text: albumInfo.artist
                        elide: Text.ElideRight
                        font.pointSize: Kirigami.Theme.defaultFont.pointSize * 1.15
                        Layout.fillWidth: true
                    }

                    Label {
                        text: albumInfo.year
                        visible: albumInfo.year > 0
                        Layout.fillWidth: true
                    }

                    Label {
                        text: qsTr("%1 songs(s)").arg(libraryListModel.count)
                        Layout.fillWidth: true
                    }
                }
            }
        }

        model: LibraryListModel {
            id: libraryListModel
        }

        delegate: ItemDelegate {
            id: itemDelegate
            width: mediaView.width
            text: "%1. %2".arg(media.track).arg(title)
            onClicked: {
                menu.songData = model.media
                menu.open()
            }
        }

        reuseItems: true
        
        Component.onCompleted: {
            libraryListModel.add(Library.getSongs(Enums.Album, albumInfo.album))
        }
    }
    
}
