import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import org.kde.kirigami as Kirigami

import dev.onigiri.ayase

Kirigami.ScrollablePage {
    id: songDetailsPage

    property var song

    title: qsTr("Details")

    ColumnLayout {
        Image {
            asynchronous: true
            fillMode: Image.PreserveAspectFit
            height: 250
            source: song.image.toString() === "" ? "qrc:album.svg" : song.image
            sourceSize.height: 255
            width: 250
            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
        }

        GridLayout {
            columns: 2
            Layout.fillWidth: true
            Layout.fillHeight: true

            Label { text: qsTr("Title") }
            TextField {
                readOnly: true
                text: song.title
                Layout.fillWidth: true
            }

            Label { text: qsTr("Artist") }
            TextField {
                readOnly: true
                text: song.artist
                Layout.fillWidth: true
            }

            Label { text: qsTr("Album") }
            TextField {
                readOnly: true
                text: song.album
                Layout.fillWidth: true
            }

            Label { text: qsTr("Album artist") }
            TextField {
                readOnly: true
                text: song.albumArtist
                Layout.fillWidth: true
            }

            Label { text: qsTr("Genre") }
            TextField {
                readOnly: true
                text: song.genre
                Layout.fillWidth: true
            }

            Label { text: qsTr("Year") }
            TextField {
                readOnly: true
                text: song.year
                Layout.fillWidth: true
            }
            
            Label { text: qsTr("Track") }
            TextField {
                readOnly: true
                text: song.track
                Layout.fillWidth: true
            }

            Label { text: qsTr("Length") }
            Label { text: Utils.timeToString(song.length) }
        }     
            
        Label { text: qsTr("Comment") }
        ScrollView {
            Layout.fillWidth: true
            Layout.preferredHeight: 200
            TextArea {
                readOnly: true
                text: song.comment
            }
        }

        Label { text: qsTr("Lyrics") }
        ScrollView {
            Layout.fillWidth: true
            Layout.preferredHeight: 200
            TextArea {
                readOnly: true
                text: song.lyrics
            }
        }
    }

    Component {
        id: textEdit

        Flickable {
            id: flick

            width: 300; height: 200;
            contentWidth: edit.paintedWidth
            contentHeight: edit.paintedHeight
            clip: true
                 
            function ensureVisible(r)
            {
                if (contentX >= r.x)
                    contentX = r.x;
                else if (contentX+width <= r.x+r.width)
                    contentX = r.x+r.width-width;
                if (contentY >= r.y)
                    contentY = r.y;
                else if (contentY+height <= r.y+r.height)
                    contentY = r.y+r.height-height;
            }

            TextEdit {
               id: edit
               width: flick.width
               focus: true
               readOnly: true
               wrapMode: TextEdit.Wrap
               onCursorRectangleChanged: flick.ensureVisible(cursorRectangle)
            }
        }
    }
}
