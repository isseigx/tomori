import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import dev.onigiri.ayase

Label {
    id: infoLabel
    elide: Text.ElideRight
    states: State {
        name: "cleared"
        when: player.state == Player.Stopped
        PropertyChanges { target: infoLabel; text: "" }
    }

    Behavior on text {
        SequentialAnimation {
            NumberAnimation { target: infoLabel;  property: "opacity"; to: 0; duration: 150; easing.type: Easing.InQuad }
            PropertyAction {}
            NumberAnimation { target: infoLabel; property: "opacity"; to: 1; duration: 150; easing.type: Easing.OutQuad }
        }
    }
    Layout.fillWidth: true
}
