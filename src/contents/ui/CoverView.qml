import QtQuick

import dev.onigiri.ayase

Image {
    id: image
    asynchronous: true
    fillMode: Image.PreserveAspectFit
    source: "qrc:cover.svg"
    sourceSize.width: width * Screen.devicePixelRatio
    
    Behavior on source {
        SequentialAnimation {
            NumberAnimation { target: image;  property: "opacity"; to: 0; duration: 150; easing.type: Easing.InQuad }
            PropertyAction {}
            NumberAnimation { target: image; property: "opacity"; to: 1; duration: 150; easing.type: Easing.OutQuad }
         }
    }

    Connections {
        target: playlistModel
        function onCurrentMediaChanged(m) {
            if (m.image.toString().length === 0) {
                image.source = "qrc:cover.svg"
            }
            else {
                image.source = m.image
            }
        }
    }

    Component.onCompleted: {
        if (player.state === Player.Playing || player.state === Player.Paused) {
            if (playlistModel.currentMedia.imagePath.toString().length !== 0) {
                image.source = playlistModel.currentSong.image
            }
        }
    }
}
