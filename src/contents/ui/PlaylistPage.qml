import QtQuick
import QtQuick.Controls
import QtQuick.Dialogs
import QtQuick.Layouts

import org.kde.kirigami as Kirigami

import dev.onigiri.ayase

Kirigami.ScrollablePage {
    id: page
    actions: [
        Kirigami.Action {
            icon.name: "media-playlist-append"
            text: qsTr("Add file")
            onTriggered: {
                var dialog = fileDialog.createObject(root)
                dialog.open()
            }
        },

        Kirigami.Action {
            icon.name: "edit-clear-list"
            text: qsTr("Clear")
            onTriggered: {
                player.stop()
                playlistModel.clear()
            }
        },

        Kirigami.Action {
            icon.name: "overflow-menu"

            Kirigami.Action {
                icon.name: "media-playlist-shuffle"
                text: qsTr("Shuffle")
                onTriggered: playlistModel.shuffle()
            }

            Kirigami.Action {
                separator: true
            }

            Kirigami.Action {
                icon.name: "document-open-folder"
                text: qsTr("Add folder...")
                onTriggered: {
                    var dialog = folderDialog.createObject(root)
                    dialog.open()
                }
            }

            Kirigami.Action {
                separator: true
            }

            Kirigami.Action {
                icon.name: "document-open-symbolic"
                text: qsTr("Open playlist...")
                onTriggered: {
                    var dialog = playlistFileDialog.createObject(root, {
                        fileMode: FileDialog.OpenFile
                    })
                    dialog.open()
                }
            }

            Kirigami.Action {
                icon.name: "document-export-symbolic"
                text: qsTr("Export playlist...")
                onTriggered: {
                    var dialog = playlistFileDialog.createObject(root, {
                        fileMode: FileDialog.SaveFile
                    })
                    dialog.open()
                }
            }
        }
    ]
    title: qsTr("Queue")

    PlaylistView {
        id: playlistView
        currentIndex: playlistModel.currentIndex
        model: playlistModel
    }

    footer: ToolBar {
        RowLayout {
            anchors.fill: parent

            Label {
                text: playlistModel.size > 0 ? ("%1 file(s) - Total time: %2").arg(playlistModel.size).arg( Utils.timeToString(playlistModel.duration)) : ""
                Layout.fillWidth: true
            }
        }
    }

    Component {
        id: fileDialog

        FileDialog {
            currentFolder: Utils.currentDir
            nameFilters: [qsTr("Music (*.flac *.mp3 *.m4a *.ogg *.oga *.opus)")]
            fileMode: FileDialog.OpenFiles
            onAccepted: {
                playlistModel.addMedia(selectedFiles)
                Utils.currentDir = currentFolder
                destroy()
            }
            onRejected: destroy()
        }
    }

    Component {
        id: folderDialog

        FolderDialog {
            currentFolder: Utils.currentDir
            onAccepted: {
                playlistModel.addFolder(selectedFolder)
                Utils.currentDir = selectedFolder
                destroy()
            }
            onRejected: destroy()
        }
    }

    Component {
        id: playlistFileDialog

        FileDialog {
            defaultSuffix: "xspf"
            currentFolder: Utils.currentDir
            nameFilters: [qsTr("XSPF playlists (*.xspf)")]

            onAccepted: {
                if (fileMode == FileDialog.SaveFile) {
                    playlistModel.load(selectedFile)
                }
                else {
                    playlistModel.open(selectedFile)
                }
                Utils.currentDir = currentFolder
                destroy()
            }

            onRejected: destroy()
        }
    }
}
