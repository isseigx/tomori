import QtQuick
import QtQuick.Controls
import QtQuick.Dialogs
import QtQuick.Layouts

import Qt.labs.folderlistmodel

import org.kde.kirigami as Kirigami

import dev.onigiri.ayase

Kirigami.ScrollablePage {
    id: filesPage

    actions: [
        Kirigami.Action {
            icon.name: "folder-music"
            tooltip: qsTr("Go to music folder")
            onTriggered: {
                folderModel.folder = "file://" + Utils.musicDir
            }
        },
        Kirigami.Action {
            icon.name: "folder-open"
            tooltip: qsTr("Open folder")
            onTriggered: {
                var dialog = fileDialog.createObject(root)
                dialog.open()
            }
        },
        Kirigami.Action {
            icon.name: "go-up"
            tooltip: qsTr("Go up")
            onTriggered: folderModel.folder = folderModel.parentFolder
        }
    ]

    title: qsTr("Files")

    ListView {
        id: folderView

        delegate: ItemDelegate {
            id: itemDelegate
            icon.name: fileIsDir ? "folder" : "audio-x-generic"
            text: fileName
            width: folderView.width
            contentItem: RowLayout {
                spacing: Kirigami.Units.smallSpacing
                Kirigami.IconTitleSubtitle {
                    title: itemDelegate.text
                    icon: icon.fromControlsIcon(itemDelegate.icon)
                    Layout.fillWidth: true
                }
                ToolButton {
                    icon.name: "overflow-menu"
                    icon.source: "qrc:more.svg"
                    onClicked: {
                        menu.fileIndex = index
                        menu.popup()
                    }
                }
            }
            onClicked: {
                if (fileIsDir) {
                    folderModel.folder = fileUrl
                }
            }
        }

        header: Kirigami.ListSectionHeader {
            label: folderModel.folder
        }

        model: folderModel
        reuseItems: true
    }

    FolderListModel {
        id: folderModel
        folder: "file://" + Utils.musicDir
        nameFilters: ["*.flac", "*.mp3", "*.m4a", "*.ogg", "*.oga","*.opus"]
    }

    Menu {
        id: menu
        property int fileIndex: -1

        MenuItem {
            text: folderModel.isFolder(menu.fileIndex) ? "Play folder" : "Play file"
            onClicked: {
                const fileUrl = folderModel.get(menu.fileIndex, "fileUrl")
                if (folderModel.isFolder(menu.fileIndex)) {
                    player.stop()
                    playlistModel.clear()
                    playlistModel.addFolder(fileUrl).then(() => {
                        playlistModel.setCurrentIndex(0)
                    })
                }
                else {
                    playlistModel.addMedia([fileUrl]).then(() => {
                        player.stop()
                        playlistModel.setCurrentIndex(playlistModel.size - 1)
                    })
                }
            }
        }

        MenuItem {
            text: folderModel.isFolder(menu.fileIndex) ? "Enqueue folder" : "Enqueue file"
            onClicked: {
                const fileUrl = folderModel.get(menu.fileIndex, "fileUrl")
                if (folderModel.isFolder(menu.fileIndex)) {
                    playlistModel.addFolder(fileUrl)
                }
                else {
                    playlistModel.addMedia([fileUrl])
                }
            }
        }
    }

    Component {
        id: fileDialog

        FolderDialog {
            currentFolder: folderModel.folder
            onAccepted: {
                folderModel.folder = selectedFolder
                Utils.currentDir = selectedFolder
                destroy()
            }
            onRejected: destroy()
        }
    }
}
