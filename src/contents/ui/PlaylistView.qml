import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import org.kde.kirigami as Kirigami

import dev.onigiri.ayase

ListView {
    id: playlistView

    delegate: listDelegate

    displaced: Transition {
        YAnimator {
            duration: Kirigami.Units.longDuration
            easing.type: Easing.InOutQuad
        }
    }

    reuseItems: true

    DropArea {
        anchors.fill: parent
        onDropped: (drop) => {
            if (drop.hasUrls) {
               playlistModel.addMedia(Utils.checkFiles(drop.urls))
            }
        }
    }

    Kirigami.MenuDialog {
        id: menuDialog
        property var mediaInfo
        property int mediaIndex

        actions: [
            Kirigami.Action {
                icon.name: "media-playback-start"
                icon.source: "qrc:play.svg"
                onTriggered: playlistModel.setCurrentIndex(menuDialog.mediaIndex)
                text: qsTr("Play")
            },
            Kirigami.Action {
                icon.name: "list-remove"
                onTriggered: playlistModel.remove(menuDialog.mediaIndex)
                text: qsTr("Remove")
            },
            Kirigami.Action {
                icon.name: "documentinfo"
                onTriggered: applicationWindow().pageStack.layers.push("qrc:/qt/qml/dev/onigiri/ayase/contents/ui/SongDetailsPage.qml", {"song" : menuDialog.mediaInfo})
                text: qsTr("Details")
            }
        ]
        title: mediaInfo?.title ? mediaInfo.title : qsTr("Media")
    }

    Component {
        id: listDelegate

        Item {
            height: listItem.height
            width: playlistView.width

            ItemDelegate {
                id: listItem
                highlighted: model.index == playlistModel.currentIndex
                width: playlistView.width
                onClicked: playlistModel.setCurrentIndex(index)
                contentItem: RowLayout {
                    Kirigami.ListItemDragHandle {
                        listItem: listItem
                        listView: playlistView
                        onMoveRequested: (oldIndex, newIndex) => {
                             playlistModel.move(oldIndex, newIndex)
                        }
                    }
                    Kirigami.TitleSubtitle {
                        title: model.title
                        subtitle: model.artist
                        Layout.fillWidth: true
                    }
                    ToolButton {
                        icon.name: "overflow-menu"
                        onClicked: {
                            menuDialog.mediaIndex = index
                            menuDialog.mediaInfo = media
                            menuDialog.open()
                        }
                    }
                }
            }
        }
    }

    Component.onCompleted: positionViewAtIndex(currentIndex, ListView.Beginning)
}
