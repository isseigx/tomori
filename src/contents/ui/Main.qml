import QtCore
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import org.kde.kirigami as Kirigami

import dev.onigiri.ayase

Kirigami.ApplicationWindow {
    id: root
    wideScreen: width > Kirigami.Units.gridUnit * 13 * 3

    height: 650
    width: 1000

    Player {
        id: player
        onMediaStatusChanged: (mediaStatus) => {
            if (mediaStatus == Player.EndReached) {
                if (playlistModel.currentIndex < playlistModel.size) {
                    playlistModel.next()
                }
            }
        }
    }

    PlaylistModel {
        id: playlistModel
        onCurrentMediaChanged: (media) => {
            if (player.state != Player.Stopped)
                player.stop()

            player.setSource(media.path)
            player.play()

            Notification.send(media)
        }
    }

    Rectangle {
        id: menuSidebar
        anchors.left: parent.left
        color: Kirigami.Theme.backgroundColor
        height: pageStack.height
        width: 220
        visible: root.wideScreen
        Kirigami.Theme.colorSet: Kirigami.Theme.Window
        Kirigami.Theme.inherit: false

        ColumnLayout {
            anchors.fill: parent

            ScrollView {
                id: scrollView
                Layout.fillWidth: true
                Layout.fillHeight: true

                ColumnLayout {
                    spacing: 0
                    width: scrollView.width

                    ItemDelegate {
                        action: Kirigami.PagePoolAction {
                            icon.name: "view-media-track"
                            page: "SongsPage.qml"
                            pagePool: mainPagePool
                            shortcut: "Alt+S"
                            text: qsTr("Songs")
                        }
                        implicitHeight: 40
                        highlighted: checked
                        Layout.fillWidth: true
                    }

                    ItemDelegate {
                        action: Kirigami.PagePoolAction {
                            icon.name: "view-media-artist"
                            page: "ArtistsPage.qml"
                            pagePool: mainPagePool
                            shortcut: "Alt+R"
                            text: qsTr("Artists")
                        }
                        implicitHeight: 40
                        highlighted: checked
                        Layout.fillWidth: true
                    }

                    ItemDelegate {
                        action: Kirigami.PagePoolAction {
                            icon.name: "view-media-album-cover"
                            page: "qrc:/qt/qml/dev/onigiri/ayase/contents/ui/AlbumsPage.qml"
                            pagePool: mainPagePool
                            shortcut: "Alt+A"
                            text: qsTr("Albums")
                        }
                        implicitHeight: 40
                        highlighted: checked
                        Layout.fillWidth: true
                    }

                    ItemDelegate {
                        action: Kirigami.PagePoolAction {
                            icon.name: "document-open-folder"
                            page: "qrc:/qt/qml/dev/onigiri/ayase/contents/ui/FilesPage.qml"
                            pagePool: mainPagePool
                            shortcut: "Alt+F"
                            text: qsTr("Files")
                        }
                        implicitHeight: 40
                        highlighted: checked
                        Layout.fillWidth: true
                    }

                    Kirigami.Separator { Layout.fillWidth: true }

                    ItemDelegate {
                        id: queueItemDelegate
                        action: Kirigami.PagePoolAction {
                            icon.name: "playlist-queue-symbolic"
                            page: "qrc:/qt/qml/dev/onigiri/ayase/contents/ui/PlaylistPage.qml"
                            pagePool: mainPagePool
                            shortcut: "Alt+Q"
                            text: qsTr("Queue")
                        }
                        implicitHeight: 40
                        highlighted: checked
                        Layout.fillWidth: true

                        Rectangle {
                            anchors.right: parent.right
                            anchors.rightMargin: 5
                            anchors.verticalCenter: parent.verticalCenter
                            color: parent.highlighted ? Kirigami.Theme.highlightedTextColor : Kirigami.Theme.highlightColor
                            height: queueCountLabel.implicitHeight + 5
                            width: queueCountLabel.implicitWidth + 7
                            radius: 2
                            visible: playlistModel.size > 0

                            Label {
                                id: queueCountLabel
                                anchors.centerIn: parent
                                color: queueItemDelegate.highlighted ? Kirigami.Theme.highlightColor : Kirigami.Theme.highlightedTextColor
                                font.bold: true
                                text: playlistModel.size
                            }
                        }
                    }
                }
            }

            ItemDelegate {
                action: Kirigami.PagePoolAction {
                    icon.name: "settings-configure"
                    page: "qrc:/qt/qml/dev/onigiri/ayase/contents/ui/SettingsPage.qml"
                    pagePool: mainPagePool
                    shortcut: "Ctrl+E"
                    text: qsTr("Settings")
                }
                implicitHeight: 40
                highlighted: checked
                Layout.fillWidth: true
            }
        }
    }

    //pageStack.initialPage: mainPagePool.loadPage("SongsPage.qml")
    pageStack.defaultColumnWidth: 25 * Kirigami.Units.gridUnit
    //pageStack.anchors.bottomMargin: !root.wideScreen && pageStack.layers.depth <= 1 ? playbackControls.height : 0
    pageStack.anchors.bottomMargin: playbackControls.height
    pageStack.anchors.leftMargin: root.wideScreen ? menuSidebar.width : 0

    title: qsTr("Ayase")
    visible: true
    
    PlaybackControls {
        id: playbackControls
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
        height: Kirigami.Units.gridUnit * 4
    }

    onWideScreenChanged: {
        while (pageStack.layers.depth > 1) {
            pageStack.layers.pop()
        }
    }

    Kirigami.PagePool {
        id: mainPagePool
    }

    Connections {
        target: Library

        function onUpdateFinished() {
            root.showPassiveNotification(qsTr("Library update finished"), "short")
        }

        function onUpdateStarted() {
            root.showPassiveNotification(qsTr("Updating Library"), "short")
        }
    }

    Connections {
        target: Mpris

        function onRaiseRequested() {
            root.raise()
        }

        function onQuitRequested() {
            root.close()
        }
    }

    Connections {
        target: Notification

        function onNextRequested() {
            playlistModel.next()
            if (playlistModel.currentIndex == -1) {
                playlistModel.next()
            }
        }

        function onPauseRequested() {
            player.play()
        }

        function onPreviousRequested() {
            playlistModel.previous()
        }
    }

    Action {
        id: playAction
        icon.name: player.state == Player.Playing ? "media-playback-pause" : "media-playback-start"
        icon.source: player.state == Player.Playing ? "qrc:pause.svg" : "qrc:play.svg"
        shortcut: "Space"
        onTriggered: {
            if (player.state == Player.Stopped)
                playlistModel.next()
            else
                player.play()
        }
    }

    Action {
        id: nextAction
        icon.name: "media-skip-forward"
        icon.source: "qrc:next.svg"
        shortcut: "Alt+N"
        onTriggered: {
            playlistModel.next()
            if (playlistModel.currentIndex == -1) {
                playlistModel.next()
            }
        }
    }

    Action {
        id: previousAction
        icon.name: "media-skip-backward"
        icon.source: "qrc:previous.svg"
        shortcut: "Alt+B"
        onTriggered: {
            if (player.state != Player.Stopped && player.position > 5000)
                player.setPosition(0)
            else
                playlistModel.previous()
        }
    }

    Action {
        id: lyricsAction
        enabled: playlistModel.currentMedia.lyrics.length !== 0
        icon.name: "view-media-lyrics"
        icon.source: "qrc:lyrics.svg"
        shortcut: "Alt+L"
        onTriggered: lyricsSheet.open()
    }

    Kirigami.OverlaySheet {
        id: lyricsSheet

        header: Kirigami.Heading {
            parent: applicationWindow().overlay
            text: playlistModel.currentMedia.title
        }

        Label {
            text: playlistModel.currentMedia.lyrics
        }
    }

    Action {
        id: stopAction
        icon.name: "media-playback-stop"
        shortcut: "Shift+Space"
        onTriggered: player.stop()
    }

    Settings {
        id: settings
        property bool updateLibraryAtStartup: true
        property int viewIndex: 0
    }

    onClosing: {
        playlistModel.save()
    }
    
    Component.onCompleted: {
        var pageUrl = "qrc:/qt/qml/dev/onigiri/ayase/contents/ui/SongsPage.qml"

        if (settings.viewIndex === 1) {
            pageUrl = "qrc:/qt/qml/dev/onigiri/ayase/contents/ui/ArtistsPage.qml"
        }
        else if (settings.viewIndex === 2) {
            pageUrl = "qrc:/qt/qml/dev/onigiri/ayase/contents/ui/AlbumsPage.qml"
        }
        else if (settings.viewIndex === 3) {
            pageUrl = "qrc:/qt/qml/dev/onigiri/ayase/contents/ui/PlaylistPage.qml"
        }
        else if (settings.viewIndex === 4) {
            pageUrl = "qrc:/qt/qml/dev/onigiri/ayase/contents/ui/PlaylistPage.qml"
        }

        root.pageStack.initialPage = mainPagePool.loadPage(pageUrl)

        playlistModel.restore()

        Mpris.setup(player, playlistModel)

        if (settings.updateLibraryAtStartup) {
            Library.update()
        }
    }
}
