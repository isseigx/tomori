import QtQuick 2.12
import QtQuick.Controls 2.12

ItemDelegate {
    id: itemDelegate
    signal rightClicked
    
    onPressAndHold: rightClicked()
    
    MouseArea {
        id: mouseArea
        
        acceptedButtons: Qt.RightButton
        
        width: parent.width
        height: parent.height
        
        onClicked: itemDelegate.rightClicked()
    }
}