import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

SimpleItemDelegate {
    id: itemDelegate
    
    property ListView listView
      
    contentItem: RowLayout {
        ColumnLayout {
            Label {
                font.bold: true
                elide: Label.ElideRight
                text: model.title ? model.title : model.path
                Layout.fillWidth: true
            }
            Label {
                elide: Label.ElideRight
                text: model.artist
                visible: model.artist !== ''
                Layout.fillWidth: true
            }
        }
        
        ToolButton {
            icon.name: "handle-sort"
            icon.source: "qrc|//icons/drag_indicator.svg"
            
            MouseArea {
                id: dragArea
            
                property bool held: false
                property int currentIndex: index
                property int mouseDownY
                
                anchors.fill: parent
            
                drag.target: held ? itemDelegate : undefined
                drag.axis: Drag.YAxis
            
                onPositionChanged: {
                    if (!pressed) {
                        return
                    }
                    var newIndex = listView.indexAt(3, 
                        listView.contentItem.mapFromItem(itemDelegate,
                        0, 0).y + dragArea.mouseDownY)
                
                    /*if (newIndex > index) {
                        newIndex += 1
                    }*/
                
                    if (newIndex > -1 && index !== newIndex) {
                        playlistModel.move(index, newIndex)
                    }
                }
            
                onPressed: {
                    held = true
                    mouseDownY = mouse.y
                }
            
                onReleased: held = false
            
                preventStealing: true
            }
        }
    }
    
    Drag.active: dragArea.held
    Drag.source: dragArea
    Drag.hotSpot.x: width / 2
    Drag.hotSpot.y: height / 2
    
    states: State {
        when: dragArea.held

        ParentChange { 
            target: itemDelegate
            parent: listView
        }
        
        AnchorChanges {
            target: itemDelegate
            anchors {
                horizontalCenter: undefined
                verticalCenter: undefined
            }
        }
    }
}
