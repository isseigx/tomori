#include <QPainter>
#include <QPixmap>
#include <QQuickImageProvider>
#include <QRandomGenerator>

class ArtistImageProvider : public QQuickImageProvider
{
public:
    ArtistImageProvider() : QQuickImageProvider(QQuickImageProvider::Pixmap)
    {
    }
    
    QPixmap requestPixmap(const QString &id, QSize *size, const QSize &requestedSize) override
    {
        int width = 32;
        int height = 32;
        
        if (size) {
            *size = QSize(width, height);
        }
        
        QPixmap pix(requestedSize.width() > 0 ? requestedSize.width() : width,
            requestedSize.height() > 0 ? requestedSize.height() : height);
        
        const QColor &color = colors.at(QRandomGenerator::global()->bounded(colors.size()));
        
        pix.fill(color);
        
        QPainter painter(&pix);
        
        QFont f = painter.font();
        f.setPointSize(12);
        
        painter.setFont(f);
        painter.setPen(color.lightness() < 128 ? Qt::white : Qt::black);
        
        if (requestedSize.isValid()) {
            painter.scale(requestedSize.width() / width, requestedSize.height() / height);
        }
        
        painter.drawText(QRectF(0, 0, width, height), Qt::AlignCenter, id[0]);
        
        return pix;
    }

private:
    QVector<QColor> colors = {
        QColor(244, 66, 54),
        QColor(233, 29, 98),
        QColor(156, 38, 176),
        QColor(103, 57, 182),
        QColor(62, 80, 180),
        QColor(32, 149, 242),
        QColor(2, 168, 244),
        QColor(1, 187, 212),
        QColor(75, 175, 79),
        QColor(139, 194, 74),
        QColor(204, 219, 56),
        QColor(255, 233, 59),
        QColor(254, 193, 7),
        QColor(255, 151, 0),
        QColor(255, 85, 33)
    };
};