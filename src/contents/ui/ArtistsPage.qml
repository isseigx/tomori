import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import org.kde.kirigami as Kirigami
import org.kde.kirigamiaddons.components as KirigamiAddons
import org.kde.kitemmodels

import dev.onigiri.ayase

Kirigami.ScrollablePage {
    id: artistsPage

    actions: [
        Kirigami.Action {
            id: customAction
            text: qsTr("Search")
            icon.name: "search"
            displayComponent: searchField
        }
    ]
    title: qsTr("Artists")

    onIsCurrentPageChanged: (currentPage) => {
        if (!currentPage && applicationWindow().pageStack.depth == 1) {
            filteredModel.filterRegularExpression = RegExp()
        }
    }

    ListView {
        id: artistsListView

        model: filteredModel

        delegate: artistDelegate

        reuseItems: true

        Component.onCompleted: libraryListModel.add(Library.getArtists())
    }

    LibraryListModel {
        id: libraryListModel
    }

    KSortFilterProxyModel {
        id: filteredModel
        filterRoleName: "artist"
        sourceModel: libraryListModel
    }

    Component {
        id: searchField
        Kirigami.SearchField {
            onTextChanged: {
                if (text !== "") {
                    filteredModel.filterRegularExpression = RegExp(text, "i")
                }
                else {
                    filteredModel.filterRegularExpression = RegExp()
                }
            }
        }
    }

    Component {
        id: artistDelegate
        ItemDelegate {
            id: itemDelegate
            text: model.artist
            width: artistsListView.width
            contentItem: RowLayout {
                spacing: Kirigami.Units.mediumSpacing

                KirigamiAddons.Avatar {
                    imageMode: KirigamiAddons.Avatar.AdaptiveImageOrInitals
                    name: model.artist
                    source: model.image
                    sourceSize: "40x40"
                }

                Label {
                    text: itemDelegate.text
                    Layout.fillWidth: true
                }
            }
            onClicked: pageStack.push("qrc:/qt/qml/dev/onigiri/ayase/contents/ui/ArtistView.qml", { "artistInfo": model.media})

            Keys.onEnterPressed: clicked()
            Keys.onReturnPressed: clicked()
        }
    }

    Connections {
        target: Library

        function onUpdateFinished() {
            libraryListModel.add(Library.getArtists())
        }
    }
}
