import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import io.onigiri.ayase 2.0

Page {
    id: nowPlayingPage
    title: qsTr("Now Playing")
    
    header: Loader {
        active: root.inPortrait
        sourceComponent: pageHeader
        visible: root.inPortrait
    }
    
    ColumnLayout {
        anchors.fill: parent
        
        ToolButton {
            icon.name: "view-more"
            icon.source: "qrc:///icons/more.svg"
            onClicked: moreMenu.popup()
            visible: !root.inPortrait
            Layout.alignment: Qt.AlignRight
        }
    
        PlaylistView {
            id: playlistView
            
            Layout.fillWidth: true
            Layout.fillHeight: true
        }
    }
    
    PlaylistMenu {
        id: moreMenu
    }
   
    Component {
        id: pageHeader
        
        ToolBar {
            RowLayout {
                anchors {
                    fill: parent
                    leftMargin: 5
                    rightMargin: 5
                }
            
                ToolButton {
                    icon.name: "open-menu"
                    icon.source: "qrc:///menu.svg"
                    visible: root.inPortrait
                    onClicked: navigationDrawer.open()
                }
                Label {
                    Layout.fillWidth: true
                    text: root.inPortrait ? nowPlayingPage.title : ""
                    font.pointSize: 14
                    elide: Text.ElideRight
                }
            
                ToolButton {
                    icon.name: "view-more"
                    icon.source: "qrc:///more.svg"
                    onClicked: moreMenu.popup()
                }
            }
        }
    }
}
