import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import org.kde.kirigami 2.15 as Kirigami

import io.onigiri.ayase 2.0

Rectangle {
    id: rect
    color: Kirigami.Theme.backgroundColor
    width: Kirigami.Units.gridUnit * 14

    Kirigami.Theme.colorSet: Kirigami.Theme.Window
    Kirigami.Theme.inherit: false

    ColumnLayout {
        anchors.fill: parent

        CoverView {
            id: coverView
            width: rect.width
            height: rect.width
            Layout.maximumHeight: rect.width
            Layout.maximumWidth: rect.width
            Layout.minimumHeight: rect.width
            Layout.minimumWidth: rect.width
        }

        InfoLabel {
            id: titleLabel
            font.weight: Font.Bold
            horizontalAlignment: Text.AlignHCenter
            maximumLineCount: 2
            text: playlistModel.currentSong.title.length === 0 ? playlistModel.currentSong.path : playlistModel.currentSong.title
            wrapMode: Text.WordWrap
        }

        InfoLabel {
            id: artistLabel
            horizontalAlignment: Text.AlignHCenter
            text: playlistModel.currentSong.artist
        }

        Kirigami.ActionToolBar {
            Layout.fillWidth: true
            actions: [
                previousAction,
                playAction,
                stopAction,
                nextAction,
                lyricsAction
            ]
            alignment: Qt.AlignCenter
            display: Button.IconOnly
        }

        RowLayout {
            Layout.fillWidth: true

            Label {
                text: Utils.timeToString(player.time)
                visible: player.state !== Player.Stopped
                Layout.leftMargin: 2
            }

            Slider {
                value: player.position
                enabled: player.state !== Player.Stopped
                onMoved: player.position = value
                Layout.fillWidth: true
            }

            Label {
                text: Utils.timeToString(player.duration)
                visible: player.state !== Player.Stopped
                Layout.rightMargin: 2
            }
        }

        Kirigami.Separator {
            Layout.fillWidth: true
        }

        PlaylistView {
            Layout.fillWidth: true
            Layout.fillHeight: true

            currentIndex: playlistModel.currentIndex
            model: playlistModel
        }
    }
}
