import QtCore
import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import org.kde.kirigami as Kirigami

import dev.onigiri.ayase

Kirigami.ScrollablePage {
    id: settingsPage

    actions: [
        Kirigami.Action {
            icon.name: "documentinfo"
            text: qsTr("Info")
            onTriggered: pageStack.layers.push(aboutPage)
            shortcut: "Alt+I"
        }
    ]

    title: qsTr("Settings")

    Kirigami.FormLayout {
        wideMode: true

        Item {
            Kirigami.FormData.isSection: true
            Kirigami.FormData.label: qsTr("Library")
        }

        Button {
            text: qsTr("Update")
            onClicked: Library.update()
        }

        Button {
            text: qsTr("Force update")
            onClicked: Library.forceUpdate()
        }

        CheckBox {
            id: updateCheckBox
            text: qsTr("Update at startup")
            checked: true
        }

        Item {
            Kirigami.FormData.isSection: true
            Kirigami.FormData.label: qsTr("Interface")
        }

        ComboBox {
            id: viewComboBox
            Kirigami.FormData.label: qsTr("First page:")
            model: [qsTr("Songs"), qsTr("Artists"), qsTr("Albums"), qsTr("Files"), qsTr("Queue")]
        }

    }

    Settings {
        property alias viewIndex: viewComboBox.currentIndex
        property alias updateLibraryAtStartup: updateCheckBox.checked
    }

    Component {
        id: aboutPage

        Kirigami.AboutPage {
            aboutData: {
                "displayName": Qt.application.name,
                "version": Qt.application.version,
                "componentName": "ayase",
                "homePage": "https://gitlab.com/isseigx/tomori/",
                "bugAddress": "https://gitlab.com/isseigx/tomori/issues",
                "shortDescription": qsTr("Simple local music player"),
                "copyrightStatement": qsTr("© 2017-%1 dev.onigiri").arg(new Date().getFullYear()),
                "authors": [
                    {
                        "name": "IsseiGX",
                        "emailAddress": "isseigx@duck.com"
                    }
                ]
            }
            title: qsTr("About")
        }
    }
}
