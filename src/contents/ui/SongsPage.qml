import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import org.kde.kirigami as Kirigami
import org.kde.kitemmodels

import dev.onigiri.ayase

Kirigami.ScrollablePage {
    id: songsPage

    actions: [
        Kirigami.Action {
            text: qsTr("Sort")
            icon.name: "view-sort"

            Kirigami.Action {
                checkable: true
                checked: true
                text: qsTr("Title")
                onTriggered: filteredModel.sortRoleName = "title"
                ActionGroup.group: sortGroup
            }

            Kirigami.Action {
                checkable: true
                text: qsTr("Artist")
                onTriggered: filteredModel.sortRoleName = "artist"
                ActionGroup.group: sortGroup
            }

            Kirigami.Action {
                checkable: true
                text: qsTr("Album")
                onTriggered: filteredModel.sortRoleName = "album"
                ActionGroup.group: sortGroup
            }

            Kirigami.Action {
                checkable: true
                text: qsTr("Year")
                onTriggered: filteredModel.sortRoleName = "year"
                ActionGroup.group: sortGroup
            }

            Kirigami.Action {
                separator: true
            }

            Kirigami.Action {
                checkable: true
                checked: true
                text: qsTr("Ascending")
                onTriggered: filteredModel.sortOrder = Qt.AscendingOrder
                ActionGroup.group: sortOrderGroup
            }

            Kirigami.Action {
                checkable: true
                text: qsTr("Descending")
                onTriggered: filteredModel.sortOrder = Qt.DescendingOrder
                ActionGroup.group: sortOrderGroup
            }
        },
        Kirigami.Action {
            id: customAction
            text: qsTr("Search")
            icon.name: "search"
            displayComponent: searchField
        }
    ]
    title: qsTr("Songs")

    onIsCurrentPageChanged: (currentPage) => {
        if (!currentPage && applicationWindow().pageStack.depth == 1) {
            filteredModel.filterRegularExpression = RegExp()
        }
    }

    ActionGroup {
        id: sortGroup
    }

    ActionGroup {
        id: sortOrderGroup
    }

    SongItemMenu {
        id: menu
    }

    ListView {
        id: songsView

        model: filteredModel
        delegate: songDelegate
        reuseItems: true
        
        Component.onCompleted: libraryListModel.add(Library.getSongs())
    }

    LibraryListModel {
        id: libraryListModel
    }

    KSortFilterProxyModel {
        id: filteredModel
        filterRoleName: "title"
        sortOrder: Qt.AscendingOrder
        sortRoleName: "title"
        sourceModel: libraryListModel
    }

    Connections {
        target: Library
        function onUpdateFinished() {
            libraryListModel.add(Library.getSongs())
        }
    }

    Component {
        id: searchField
        Kirigami.SearchField {
            onTextChanged: {
                if (text !== "") {
                    filteredModel.filterRegularExpression = RegExp(text, "i")
                }
                else {
                    filteredModel.filterRegularExpression = RegExp()
                }
            }
        }
    }

    Component {
        id: songDelegate

        ItemDelegate {
            id: itemDelegate
            width: songsView.width

            contentItem: RowLayout {
                spacing: Kirigami.Units.smallSpacing

                Image {
                    asynchronous: true
                    clip: true
                    fillMode: Image.PreserveAspectCrop
                    height: itemDelegate.height
                    source: image === "" ? "qrc:cover.svg" : image
                    sourceSize: "40x40"
                    width: height
                }
                Kirigami.TitleSubtitle {
                    title: model.title === "" ? model.path : model.title
                    subtitle: model.artist + " · " + model.album
                    selected: itemDelegate.highlighted || itemDelegate.down
                    Layout.fillWidth: true
                }
            }

            onClicked: {
                menu.songData = model.media
                menu.open()
            }
            Keys.onEnterPressed: clicked()
            Keys.onReturnPressed: clicked()
        }
    }
}

