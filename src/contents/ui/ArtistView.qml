import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import org.kde.kirigami as Kirigami
import org.kde.kirigamiaddons.components as KirigamiAddons

import dev.onigiri.ayase

Kirigami.ScrollablePage {
    id: artistView

    property var artistInfo

    title: qsTr("Artist")

    actions: [
        Kirigami.Action {
            icon.name: "media-playback-start"
            text: qsTr("Play")
            onTriggered: {
                player.stop()
                playlistModel.clear()
                playlistModel.addMedia(libraryListModel.mediaList)
                playlistModel.currentIndex = 0
                player.play()
            }
        },
        Kirigami.Action {
            icon.name: "media-playlist-append"
            text: qsTr("Enqueue")
            onTriggered: playlistModel.addMedia(libraryListModel.mediaList)
        }
    ]

    SongItemMenu {
        id: menu
    }

    ListView {
        id: songsListView
        header: Pane {
            width: songsListView.width

            RowLayout {
                anchors.fill: parent
                anchors.margins: Kirigami.Units.smallSpacing
                spacing: Kirigami.Units.largeSpacing

                KirigamiAddons.Avatar {
                    imageMode: KirigamiAddons.Avatar.AdaptiveImageOrInitals
                    name: artistInfo.artist
                    source: artistInfo.image
                    sourceSize: "64x64"
                }

                ColumnLayout {
                    spacing: Kirigami.Units.mediumSpacing
                    Layout.fillWidth: true

                    Label {
                        text: artistInfo.artist
                        elide: Text.ElideRight
                        font.pointSize: Kirigami.Theme.defaultFont.pointSize * 1.35
                        font.weight: Font.DemiBold
                        Layout.fillWidth: true
                    }

                    Label {
                        text: qsTr("%1 songs(s)").arg(libraryListModel.count)
                        Layout.fillWidth: true
                    }
                }
            }
        }
        delegate: ItemDelegate {
            id: itemDelegate
            width: songsListView.width
            text: "%1. %2".arg(media.track).arg(title)
            onClicked: {
                menu.songData = model.media
                menu.open()
            }
        }

        model: LibraryListModel {
            id: libraryListModel
        }

        section.property: "album"
        section.criteria: ViewSection.FullString
        section.delegate: Kirigami.ListSectionHeader {
            label: section
        }

        reuseItems: true
        Component.onCompleted: libraryListModel.add(Library.getSongs(Enums.Artist, artistInfo.artist))
    }
}
