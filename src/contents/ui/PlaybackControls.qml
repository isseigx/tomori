import QtQuick
import QtQuick.Controls
import QtQuick.Effects
import QtQuick.Layouts

import org.kde.kirigami as Kirigami

import dev.onigiri.ayase

Control {
    id: base
    readonly property bool isWidescreen: width >= Kirigami.Units.gridUnit * 50

    Kirigami.ImageColors {
        id: imageColors
        source: playlistModel.currentMedia.image ? playlistModel.currentMedia.image : "qrc:cover.svg"
    }

    background: Item {
        Rectangle {
            anchors.fill: parent
            color: playlistModel.currentMedia.image.toString() !== "" ? Qt.rgba(25, 25, 30, 1) : Kirigami.Theme.backgroundColor
        }

        Image {
            anchors.fill: parent
            asynchronous: true
            fillMode: Image.PreserveAspectCrop
            source: playlistModel.currentMedia.image.toString() === "" ? "qrc:cover.svg" : playlistModel.currentMedia.image
            opacity: 0.2
        }

        layer.enabled: true
        layer.effect: MultiEffect {
            autoPaddingEnabled: false
            blurEnabled: true
            blur: 1.0
            blurMultiplier: 3.0
            brightness: -0.6
            saturation: 1.1
        }
    }

    ColumnLayout {
        anchors.fill: parent
        spacing: 2

        Slider {
            id: timeSlider
            background: Rectangle {
                x: timeSlider.leftPadding
                y: timeSlider.topPadding + timeSlider.availableHeight / 2 - height / 2
                implicitWidth: 200
                implicitHeight: 6
                width: timeSlider.availableWidth
                height: implicitHeight
                color: Qt.rgba(0, 0, 0, 0.4)

                Rectangle {
                    width: timeSlider.visualPosition * parent.width
                    height: parent.height
                    color: imageColors.highlight
                }
            }
            handle: Item {
                x: timeSlider.leftPadding + timeSlider.visualPosition * (timeSlider.availableWidth - width)
                y: timeSlider.topPadding + timeSlider.availableHeight / 2 - height / 2
                implicitWidth: 6
                implicitHeight: 6
            }
            enabled: player.state !== Player.Stopped
            onMoved: player.setPosition(value * player.duration)
            value: player.position / player.duration
            to: 1
            Layout.fillWidth: true
            Layout.maximumHeight: 5
        }

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true

            CoverView {
                id: coverView

                anchors.left: parent.left
                anchors.leftMargin: 5
                anchors.verticalCenter: parent.verticalCenter

                height: base.height - Kirigami.Units.largeSpacing * 2
                width: base.height - Kirigami.Units.largeSpacing * 2
                visible: playlistModel.currentIndex != -1
            }

            ColumnLayout {
                id: columnInfo

                anchors {
                    left: coverView.right
                    leftMargin: 5
                    right: buttonsRow.left
                    rightMargin: 5
                    verticalCenter: parent.verticalCenter
                }

                spacing: 2

                InfoLabel {
                    id: titleLabel
                    color: imageColors.closestToWhite
                    font.weight: Font.Bold
                    elide: Text.ElideRight
                    text: playlistModel.currentMedia.title.length === 0 ? playlistModel.currentMedia.path : playlistModel.currentMedia.title
                    Layout.fillWidth: true
                }

                InfoLabel {
                    id: artistLabel
                    color: imageColors.closestToWhite
                    elide: Text.ElideRight
                    text: playlistModel.currentMedia.artist
                    Layout.fillWidth: true
                }

                InfoLabel {
                    id: albumLabel
                    color: imageColors.closestToWhite
                    elide: Text.ElideRight
                    text: playlistModel.currentMedia.album
                    Layout.fillWidth: true
                }
            }

            RowLayout {
                id: buttonsRow

                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: parent.top

                PlaybackButton {
                    action: previousAction
                    iconColor: imageColors.closestToWhite
                }

                PlaybackButton {
                    action: playAction
                    iconColor: imageColors.closestToWhite
                }

                PlaybackButton {
                    action: nextAction
                    iconColor: imageColors.closestToWhite
                }
            }

            RowLayout {
                anchors.right: parent.right
                anchors.rightMargin: 5
                anchors.verticalCenter: parent.verticalCenter

                Label {
                    color: imageColors.closestToWhite
                    text: Utils.timeToString(player.position) + "/" + Utils.timeToString(player.duration)
                    visible: player.state !== Player.Stopped
                }

                PlaybackButton {
                    id: lyricsButton
                    action: lyricsAction
                    iconColor: imageColors.closestToWhite
                    visible: action.enabled
                }
            }
        }
    }
}
