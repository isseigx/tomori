import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import org.kde.kirigami 2.15 as Kirigami

import io.onigiri.ayase 2.0

Kirigami.Page {
    id: page
    title: qsTr("Now Playing")

    ColumnLayout {
        anchors.fill: parent

        Item { Layout.fillHeight: true }

        CoverView {
            id: cover
            height: 250
            width: 250
            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
            Layout.bottomMargin: 5
            Layout.preferredHeight: 250
            Layout.preferredWidth: 250
        }

        RowLayout {
            Layout.fillWidth: true

            ColumnLayout {
                Layout.fillWidth: true

                InfoLabel {
                    id: titleLabel
                    font.pointSize: Kirigami.Theme.defaultFont.pointSize * 1.35
                    font.weight: Font.Bold
                    maximumLineCount: 2
                    text: playlistModel.currentSong.title.length === 0 ? playlistModel.currentSong.path : playlistModel.currentSong.title
                    wrapMode: Text.WordWrap
                }

                InfoLabel {
                    id: artistLabel
                    text: playlistModel.currentSong.artist
                }

                InfoLabel {
                    id: albumLabel
                    color: Kirigami.Theme.disabledTextColor
                    font.pointSize: 9
                    text: playlistModel.currentSong.album
                }
            }

            RowLayout {
                Layout.fillHeight: true

                ToolButton {
                    action: lyricsAction
                }
            }
        }

        RowLayout {
            Layout.bottomMargin: 5
            Layout.topMargin: 5
            ToolButton {
                action: playAction
            }

            ToolButton {
                action: previousAction
            }

            ToolButton {
                action: nextAction
            }
        }

        RowLayout {
            Layout.fillWidth: true

            Label {
                text: Utils.timeToString(player.time)
                visible: player.state !== Player.Stopped
            }

            Slider {
                value: player.position
                enabled: player.state !== Player.Stopped
                onMoved: player.position = value
                Layout.fillWidth: true
            }

            Label {
                text: Utils.timeToString(player.duration)
                visible: player.state !== Player.Stopped
            }
        }

        RowLayout {
            Layout.fillWidth: true
            Layout.topMargin: 5

            Label {
                color: Kirigami.Theme.highlightColor
                text: qsTr("Playing %1 of %2").arg(playlistModel.currentIndex + 1).arg(playlistModel.count)
                Layout.fillWidth: true
            }

            ToolButton {
                action: Kirigami.Action {
                    iconName: "view-media-playlist"
                    iconSource: "qrc:///icons/queue_music.svg"
                    shortcut: "Shift+L"
                    tooltip: qsTr("View playlist")
                    onTriggered: pageStack.layers.push(playlistPage)
                }
            }
        }

        Item { Layout.fillHeight: true }
    }

    Component {
        id: playlistPage

        Kirigami.Page {
            title: qsTr("Playlist")

            PlaylistView {
                anchors.fill: parent
                model: playlistModel
            }
        }
    }
}
