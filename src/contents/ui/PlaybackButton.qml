import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import org.kde.kirigami as Kirigami

Button {
    id: control

    hoverEnabled: true

    Kirigami.Theme.colorSet: Kirigami.Theme.Complementary
    Kirigami.Theme.inherit: false

    property alias customBackground: rect
    property alias iconColor: iconItem.color

    background: Rectangle {
        id: rect
        color: {
            if (parent.down) {
                Qt.rgba(0, 0, 0, 0.7)
            }
            else if (parent.hovered) {
                Qt.rgba(0, 0, 0, 0.4)
            }
            else {
                Qt.rgba(0, 0, 0, 0.2)
            }
        }
        radius: 5
    }

    contentItem: Item {
        Kirigami.Icon {
            id: iconItem
            anchors.fill: parent
            anchors.margins: 5
            color: "white"
            height: Kirigami.Units.gridUnit
            width: Kirigami.Units.gridUnit
            source: control.icon.name
        }
    }

    implicitHeight: 40
    implicitWidth: 40
}
