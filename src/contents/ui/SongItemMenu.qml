import QtQuick
import QtQuick.Controls

import org.kde.kirigami as Kirigami

import dev.onigiri.ayase

Kirigami.MenuDialog {
    property media songData

    title: songData?.title

    actions: [
        Kirigami.Action {
            icon.name: "media-playback-start"
            icon.source: "qrc:play.svg"
            onTriggered: {
                playlistModel.addMedia(songData)
                playlistModel.setCurrentIndex(playlistModel.size - 1)
            }
            text: qsTr("Play")
        },
        Kirigami.Action {
            icon.name: "media-playback-playing"
            onTriggered: playlistModel.insert(playlistModel.currentIndex + 1, songData)
            text: qsTr("Play next")
        },
        Kirigami.Action {
            icon.name: "media-playlist-append"
            icon.source: "qrc:queue_music.svg"
            onTriggered: playlistModel.addMedia(songData)
            text: qsTr("Enqueue")
        },
        Kirigami.Action {
            icon.name: "documentinfo"
            onTriggered: applicationWindow().pageStack.layers.push("qrc:/qt/qml/dev/onigiri/ayase/contents/ui/SongDetailsPage.qml", {"song" : songData})
            text: qsTr("Details")
        }
    ]
}
