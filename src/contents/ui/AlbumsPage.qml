import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import org.kde.kirigami as Kirigami
import org.kde.kitemmodels

import dev.onigiri.ayase

Kirigami.ScrollablePage {
    id: albumsPage

    actions: [
            Kirigami.Action {
            id: customAction
            text: qsTr("Search")
            icon.name: "search"
            displayComponent: searchField
        }
    ]
    title: qsTr("Albums")

    onIsCurrentPageChanged: (currentPage) => {
        if (!currentPage && applicationWindow().pageStack.depth == 1) {
            filteredModel.filterRegularExpression = RegExp()
        }
    }

    GridView {
        id: albumsGridView

        readonly property int columns: {
            const minFromWidth = Math.floor(width / (Kirigami.Units.gridUnit * 5))
            const maxFromWidth = Math.ceil(width / (Kirigami.Units.gridUnit * 10))
            return Math.max(2,Math.min(minFromWidth,maxFromWidth))
        }

        cellWidth: Math.floor(width / columns)
        cellHeight: cellWidth + Kirigami.Units.gridUnit * 2 + Kirigami.Units.mediumSpacing

        model: filteredModel

        delegate: CoverDelegate {
            width: albumsGridView.cellWidth
            height: albumsGridView.cellHeight

            onClicked: pageStack.push("qrc:/qt/qml/dev/onigiri/ayase/contents/ui/AlbumView.qml", { "albumInfo": model.media })
        }

        Component.onCompleted: libraryListModel.add(Library.getAlbums())
    }

    LibraryListModel {
        id: libraryListModel
    }

    KSortFilterProxyModel {
        id: filteredModel
        filterRoleName: "album"
        sourceModel: libraryListModel
    }

    Component {
        id: searchField
        Kirigami.SearchField {
            onTextChanged: {
                if (text !== "") {
                    filteredModel.filterRegularExpression = RegExp(text, "i")
                }
                else {
                    filteredModel.filterRegularExpression = RegExp()
                }
            }
        }
    }

    Connections {
        target: Library
        function onUpdateFinished() {
            libraryListModel.add(Library.getAlbums())
        }
    }
}
