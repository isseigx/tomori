#include "Tomori.h"

#include <QtDBus/QDBusInterface>
#include <QDir>
#include <QDebug>
#include <QStandardPaths>
#include <QUrl>

#include <MimeChecker.h>
#include <PlaylistParser.h>
#include <MPRIS.h>

Tomori::Tomori(QObject *parent) : QObject(parent)
{
    cacheLocation();
    createDataLocation();
}

Tomori::~Tomori()
{
    if (player) {
        player->stop();
        playlistmodel->saveCurrentPlaylist();
        delete player;
        delete noti;
        delete playlistmodel;
    }
    qDebug() << "Bye!";
}

void Tomori::createPlayer()
{
    player = new Player();

    noti = new Notification(this);

    playlistmodel = new PlaylistModel(this);
    playlistmodel->setPlayer(player);
    playlistmodel->restorePlaylist();

    connectActions();
}

void Tomori::connectActions()
{
    connect(playlistmodel, &PlaylistModel::currentSongChanged, noti, qOverload<const song_t &>(&Notification::notify));
}

void Tomori::addFile(const QString &file)
{
    if(!MimeChecker::isAudioFileSupported(file))
        return;
    
    QDBusInterface dbus(SERVICE, PATH, INTERFACE, QDBusConnection::sessionBus());
    
    QDBusMessage msg = dbus.call(QStringLiteral("OpenUri"), file);
    
    if (msg.type() == QDBusMessage::ErrorMessage)
        qDebug() << tr("D-Bus Error: ") << msg.errorMessage();
}

void Tomori::initDBus()
{
    new MPRIS2(player, playlistmodel, this);
    
    QDBusConnection::sessionBus().connect(QStringLiteral("org.freedesktop.Notifications"),
                                                         QStringLiteral("/org/freedesktop/Notifications"),
                                                         QStringLiteral("org.freedesktop.Notifications"),
                                                         QStringLiteral("ActionInvoked"),
                                                         this, SLOT(onDBusActionInvoked(uint,QString)));
}

bool Tomori::testConnection()
{
    QDBusInterface dbus(SERVICE, PATH, INTERFACE, QDBusConnection::sessionBus());

    return dbus.isValid();
}

void Tomori::playFile(const QString &file)
{
    if(!MimeChecker::isAudioFileSupported(file)) {
        return;
    }

    playlistmodel->addFiles({QUrl::fromLocalFile(file)});
    playlistmodel->setCurrentIndex(playlistmodel->rowCount({}) - 1);
}

void Tomori::cacheLocation()
{
    const auto cachedir = QStandardPaths::standardLocations(
        QStandardPaths::CacheLocation
    ).constFirst();

    QDir dir;

    if (!dir.exists(cachedir)) {
        dir.mkpath(cachedir);
    }
}

void Tomori::createDataLocation()
{
    const auto dataLocation = QStandardPaths::standardLocations(
        QStandardPaths::AppDataLocation
    ).constFirst();

    QDir dir;

    if (!dir.exists(dataLocation)) {
        dir.mkpath(dataLocation);
        dir.cd(dataLocation);
        dir.mkdir(QStringLiteral("playlists"));
        dir.mkdir(QStringLiteral("covers"));
    }
}

void Tomori::playPause()
{
    QDBusInterface dbus(SERVICE, PATH, INTERFACE, QDBusConnection::sessionBus());
    dbus.call(QStringLiteral("PlayPause"));
}

void Tomori::next()
{
    QDBusInterface dbus(SERVICE, PATH, INTERFACE, QDBusConnection::sessionBus());
    dbus.call(QStringLiteral("Next"));
}

void Tomori::prev()
{
    QDBusInterface dbus(SERVICE, PATH, INTERFACE, QDBusConnection::sessionBus());
    dbus.call(QStringLiteral("Previous"));
}

void Tomori::stop()
{
    QDBusInterface dbus(SERVICE, PATH, INTERFACE, QDBusConnection::sessionBus());
    dbus.call(QStringLiteral("Stop"));
}

void Tomori::onDBusActionInvoked(uint id, const QString &action)
{
    if (action == QStringLiteral("prev")) {
        playlistmodel->previous();
    }
    else if (action == QStringLiteral("next")) {
        playlistmodel->next();
    }
    else if (action == QStringLiteral("pause")) {
        player->play();
    }
}
