#ifndef MPRIS_H
#define MPRIS_H

#include <memory>

#include <QObject>
#include <QQmlEngine>

#include "MediaPlayer2.h"
#include "MediaPlayer2Player.h"

#include "core/Player.h"
#include "core/PlaylistModel.h"

class Mpris : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_SINGLETON

public:
    static Mpris &instance()
    {
        static Mpris _instance;
        return _instance;
    }

    static Mpris *create(QQmlEngine *engine, QJSEngine *)
    {
        engine->setObjectOwnership(&instance(), QQmlEngine::CppOwnership);
        return &instance();
    }

    ~Mpris();

    Q_INVOKABLE bool registered() const;

public slots:
    void setup(Player *player, PlaylistModel *playlist);
    void playUri(const QString &uri);

signals:
    void quitRequested();
    void raiseRequested();

private:
    Mpris(QObject *parent = nullptr);

    QUrl m_pendingUri;
};

#endif //MPRIS_H
