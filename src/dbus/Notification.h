#ifndef NOTIFICATION_H
#define NOTIFICATION_H

#include <QObject>
#include <QQmlEngine>

#include <QCoroQmlTask>

#include <Media.h>

class Notification : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_SINGLETON

public:
    static Notification &instance()
    {
        static Notification _instance;
        return _instance;
    }

    static Notification *create(QQmlEngine *engine, QJSEngine *)
    {
        engine->setObjectOwnership(&instance(), QQmlEngine::CppOwnership);
        return &instance();
    }

    ~Notification() = default;

    Q_INVOKABLE QCoro::QmlTask send(const media_t &media);

signals:
    void nextRequested();
    void pauseRequested();
    void previousRequested();

private:
    explicit Notification(QObject *parent = nullptr);

    QCoro::Task<> sendNotification(const media_t &media);
};

#endif //NOTIFICATION_H
