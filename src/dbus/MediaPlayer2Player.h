#ifndef MEDIAPLAYER2PLAYER_H
#define MEDIAPLAYER2PLAYER_H

#include <QDBusAbstractAdaptor>
#include <QDBusObjectPath>

#include "core/Player.h"
#include "core/PlaylistModel.h"

class MediaPlayer2Player : public QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.mpris.MediaPlayer2.Player");

    Q_PROPERTY(QString PlaybackStatus READ PlaybackStatus);
    Q_PROPERTY(QString LoopStatus READ LoopStatus WRITE setLoopStatus);
    Q_PROPERTY(double Rate READ Rate WRITE setRate);
    Q_PROPERTY(bool Shuffle READ Shuffle WRITE setShuffle);
    Q_PROPERTY(QVariantMap Metadata READ Metadata);
    Q_PROPERTY(double Volume READ Volume WRITE setVolume);
    Q_PROPERTY(qlonglong Position READ Position );
    Q_PROPERTY(double MinimumRate READ MinimumRate);
    Q_PROPERTY(double MaximumRate READ MaximumRate);
    Q_PROPERTY(bool CanGoNext READ CanGoNext);
    Q_PROPERTY(bool CanGoPrevious READ CanGoPrevious);
    Q_PROPERTY(bool CanPlay READ CanPlay);
    Q_PROPERTY(bool CanPause READ CanPause);
    Q_PROPERTY(bool CanSeek READ CanSeek);
    Q_PROPERTY(bool CanControl READ CanControl);

public:
    explicit MediaPlayer2Player(Player *p, PlaylistModel *m, QObject *parent);
    ~MediaPlayer2Player();

    QString PlaybackStatus() const;

    QString LoopStatus() const;
    Q_NOREPLY void setLoopStatus(const QString &loopStatus) const;

    double Rate() const;
    Q_NOREPLY void setRate(double rate) const;

    bool Shuffle() const;
    void setShuffle(bool shuffle) const;

    QVariantMap Metadata() const;

    double Volume() const;

    qlonglong Position() const;

    double MinimumRate() const;
    double MaximumRate() const;

    bool CanGoNext() const;
    bool CanGoPrevious() const;
    bool CanPlay() const;
    bool CanPause() const;
    bool CanSeek() const;
    bool CanControl() const;

public slots:
    Q_NOREPLY void Next();
    Q_NOREPLY void Previous();
    Q_NOREPLY void Pause();
    Q_NOREPLY void PlayPause();
    Q_NOREPLY void Stop();
    Q_NOREPLY void OpenUri(const QString &Uri) const;
    Q_NOREPLY void Play();
    Q_NOREPLY void Seek(qlonglong Offset) const;
    Q_NOREPLY void SetPosition(const QDBusObjectPath& TrackId, qlonglong Position) const;
    Q_NOREPLY void setVolume(double volume);

signals:
    void Seeked(qlonglong Position);

private slots:
    void currentMediaChanged(const media_t &media);
    void playerState(Player::PlayerState);
    //TODO: void totalTimeChanged();
    void seekableChanged(bool seekable);
    //TODO: void volumeChanged(float newVol) const;
    //void seeked(int newPos) const;
    
private:
    void propertiesChanged(const QString &propertyName, const QVariant &value) const;
    void connectActions();
    
    Player *m_player;
    PlaylistModel *m_playlist;
    
    media_t m_metadata;
};

#endif //MEDIAPLAYER2PLAYER_H
