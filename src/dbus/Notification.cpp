#include "Notification.h"

#include <QFile>
#include <QPixmap>
#include <QtConcurrent>

#include <KNotification>

#include <QCoroIODevice>

Notification::Notification(QObject *parent) : QObject(parent)
{
}

QCoro::QmlTask Notification::send(const media_t &media)
{
    return sendNotification(media);
}

QCoro::Task<> Notification::sendNotification(const media_t &media)
{
    auto notification = new KNotification(QStringLiteral("TrackChanged"));

    if (!media.image.isEmpty()) {
        QFile file(media.image.toLocalFile());
        QPixmap pix;
        if (file.open(QIODevice::ReadOnly)) {
            const QByteArray content = co_await file;
            if (pix.loadFromData(content)) {
                notification->setPixmap(pix);
            }
        }
    }

    if (!media.artist.isEmpty()) {
        notification->setText(media.artist);
        notification->setTitle(media.title);
    }
    else {
        notification->setText(media.title);
    }

    auto prevAction = notification->addAction(tr("Previous"));
    connect(prevAction, &KNotificationAction::activated, this, &Notification::previousRequested);

    auto pauseAction = notification->addAction(tr("Pause"));
    connect(pauseAction, &KNotificationAction::activated, this, &Notification::pauseRequested);

    auto nextAction = notification->addAction(tr("Next"));
    connect(nextAction, &KNotificationAction::activated, this, &Notification::nextRequested);

    notification->sendEvent();
}
