#include "MediaPlayer2.h"

#include <QApplication>
#include <QDBusConnection>
#include <QDBusMessage>

MediaPlayer2::MediaPlayer2(QObject *parent) :
QDBusAbstractAdaptor(parent)
{
}

MediaPlayer2::~MediaPlayer2()
{
}

bool MediaPlayer2::CanRaise() const
{
    return true;
}

bool MediaPlayer2::CanQuit() const
{
    return true;
}

bool MediaPlayer2::CanSetFullScreen() const
{
    return false;
}

QString MediaPlayer2::DesktopEntry() const
{
    return qApp->desktopFileName();
}

bool MediaPlayer2::FullScreen() const
{
    return false;
}

bool MediaPlayer2::HasTrackList() const
{
    return false;
}

QString MediaPlayer2::Identity() const
{
    return qApp->applicationName();
}

void MediaPlayer2::Quit()
{
    emit quitPlayer();
}

void MediaPlayer2::Raise()
{
    emit raisePlayer();
}

QStringList MediaPlayer2::SupportedUriSchemes() const
{
    return QStringList(QStringLiteral("file"));
}

QStringList MediaPlayer2::SupportedMimeTypes() const
{
    QStringList mime;
    mime << QStringLiteral("audio/ogg")
         << QStringLiteral("audio/mpeg")
         << QStringLiteral("audio/flac")
         << QStringLiteral("audio/mp4")
         << QStringLiteral("audio/opus")
         << QStringLiteral("audio/x-vorbis-+ogg")
         << QStringLiteral("audio/x-mp3")
         << QStringLiteral("audio/x-mpeg")
         << QStringLiteral("audio/x-m4a")
         << QStringLiteral("audio/x-flac");

    return mime;
}
