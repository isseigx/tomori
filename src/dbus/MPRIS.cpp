#include "MPRIS.h"

#include <QDBusConnection>
#include <QDBusInterface>
#include <QDir>

#include "core/MimeChecker.h"
#include "MediaPlayer2.h"
#include "MediaPlayer2Player.h"

using namespace Qt::Literals::StringLiterals;

static const QString interface = u"org.mpris.MediaPlayer2.Player"_s;
static const QString path = u"/org/mpris/MediaPlayer2"_s;
static const QString service = u"org.mpris.MediaPlayer2.ayase"_s;

Mpris::Mpris(QObject *parent) : QObject(parent)
{

}

Mpris::~Mpris()
{

}

void Mpris::playUri(const QString &uri)
{
    const auto url = QUrl::fromUserInput(uri, QDir::currentPath());

    if (url.isLocalFile()) {
        if (!MimeChecker::isAudioFileSupported(uri)) {
            return;
        }
    }

    if (!registered()) {
        m_pendingUri = url;
        return;
    }

    QDBusInterface iface(service, path, interface, QDBusConnection::sessionBus());
    auto message = iface.call(u"OpenUri"_s, uri);

    if (message.type() == QDBusMessage::ErrorMessage) {
        qDebug("[Ayase DBus error]: %s", message.errorMessage().toUtf8().constData());
    }
}

bool Mpris::registered() const
{
    QDBusInterface iface{service, path, interface, QDBusConnection::sessionBus()};

    return iface.isValid();
}

void Mpris::setup(Player *player, PlaylistModel *playlist)
{
    auto success = QDBusConnection::sessionBus().registerService(service);

    if (!success) {
        qDebug("[Ayase DBus]: could not register Mpris2 service");
    }

    new MediaPlayer2(this);
    new MediaPlayer2Player(player, playlist, this);

    QDBusConnection::sessionBus().registerObject(path, this, QDBusConnection::ExportAdaptors);

    if (m_pendingUri.isEmpty()) {
        return;
    }

    playlist->addMedia(m_pendingUri);
    playlist->setCurrentIndex(playlist->size() - 1);
    m_pendingUri.clear();
}
