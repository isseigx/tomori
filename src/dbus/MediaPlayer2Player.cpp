#include "MediaPlayer2Player.h"

#include <QDBusConnection>
#include <QDBusMessage>
#include <QDir>
#include <QUrl>

#include <Media.h>

using namespace Qt::Literals::StringLiterals;

MediaPlayer2Player::MediaPlayer2Player(Player *p, PlaylistModel *m, QObject *parent) : QDBusAbstractAdaptor(parent),
m_player(p),
m_playlist(m)
{
    connectActions();
}

MediaPlayer2Player::~MediaPlayer2Player()
{
}

void MediaPlayer2Player::Next()
{
    m_playlist->next();

    if (m_playlist->currentIndex() == -1) {
        m_playlist->next();
    }
}

void MediaPlayer2Player::Previous()
{
    if (m_player->state() != Player::Stopped && m_player->position() > 5000) {
        m_player->setPosition(0);
    }
    else {
        m_playlist->previous();
    }
}

void MediaPlayer2Player::Pause()
{
    m_player->play();
}

void MediaPlayer2Player::PlayPause()
{
    m_player->play();
}

void MediaPlayer2Player::Stop()
{
    m_player->stop();
}

void MediaPlayer2Player::Play()
{
    m_player->play();
}

void MediaPlayer2Player::SetPosition(const QDBusObjectPath& TrackId, qlonglong Position) const
{
    //TODO
    Q_UNUSED(TrackId);
    Q_UNUSED(Position);
}

void MediaPlayer2Player::OpenUri(const QString &Uri) const
{
    m_playlist->addMedia(QUrl::fromUserInput(Uri, QDir::currentPath()));
    m_playlist->setCurrentIndex(m_playlist->size() - 1);
}

QString MediaPlayer2Player::PlaybackStatus() const
{
    auto state = m_player->state();

    if (state == Player::Playing) {
        return u"Playing"_s;
    }
    else if (state == Player::Paused) {
        return u"Paused"_s;
    }
    else {
        return u"Stopped"_s;
    }
}

QString MediaPlayer2Player::LoopStatus() const
{
    return u"None"_s;
}

void MediaPlayer2Player::setLoopStatus(const QString &loopStatus) const
{
    Q_UNUSED(loopStatus);
}

double MediaPlayer2Player::Rate() const
{
    return 1.0;
}

void MediaPlayer2Player::setRate(double rate) const
{
    Q_UNUSED(rate);
}

bool MediaPlayer2Player::Shuffle() const
{
    return false;
}

void MediaPlayer2Player::setShuffle(bool shuffle) const
{
    Q_UNUSED(shuffle);
}

QVariantMap MediaPlayer2Player::Metadata() const
{
    QVariantMap metadata;

    //TODO: set mpris:trackid
    metadata[u"mpris:length"_s] = qlonglong(m_player->duration()) * 1000;

    if (!m_metadata.image.isEmpty()) {
        metadata[u"mpris:artUrl"_s] = m_metadata.image.toLocalFile();
    }

    if (!m_metadata.album.isEmpty()) {
        metadata[u"xesam:"_s] = m_metadata.album;
    }

    if (!m_metadata.albumArtist.isEmpty()) {
        metadata[u"xesam:albumArtist"_s] = m_metadata.albumArtist;
    }

    if (!m_metadata.artist.isEmpty()) {
        metadata[u"xesam:artist"_s] = m_metadata.artist;
    }

    if (!m_metadata.lyrics.isEmpty()) {
        metadata[u"xesam:asText"_s] = m_metadata.lyrics;
    }

    if (!m_metadata.comment.isEmpty()) {
        metadata[u"xesam:comment"_s] = m_metadata.comment;
    }

    if (!m_metadata.genre.isEmpty()) {
        metadata[u"xesam:genre"_s] = m_metadata.genre;
    }

    metadata[u"xesam:title"_s] = m_metadata.title;
    metadata[u"xesam:track"_s] = m_metadata.track;

    if (m_metadata.path.isLocalFile()) {
        metadata[u"xesam:url"_s] = m_metadata.path.toLocalFile();
    }
    else {
        metadata[u"xesam:url"_s] = m_metadata.path.toString();
    }

    return metadata;
}

double MediaPlayer2Player::Volume() const
{
    return 100.0;
}

void MediaPlayer2Player::setVolume(double volume)
{
    //TODO
    Q_UNUSED(volume);
}

qlonglong MediaPlayer2Player::Position() const
{
    return m_player->position() * 1000; // convert milliseconds to microseconds
}

double MediaPlayer2Player::MinimumRate() const
{
    return 1.0;
}

double MediaPlayer2Player::MaximumRate() const
{
    return 1.0;
}

bool MediaPlayer2Player::CanSeek() const
{
    return m_player->seekable();
}

void MediaPlayer2Player::Seek(qlonglong Offset) const
{
    m_player->setPosition(Offset / 1000); // convert microseconds to milliseconds
}

bool MediaPlayer2Player::CanControl() const
{
    return true;
}


void MediaPlayer2Player::propertiesChanged(const QString &propertyName, const QVariant &value) const
{
    QVariantMap properties;
    properties[propertyName] = value;

    QDBusMessage msg = QDBusMessage::createSignal(u"/org/mpris/MediaPlayer2"_s,
                                                  u"org.freedesktop.DBus.Properties"_s,
                                                  u"PropertiesChanged"_s);

    msg << u"org.mpris.MediaPlayer2.Player"_s; // dbus interface name
    msg << properties; // properties to send
    msg << QStringList();

    QDBusConnection::sessionBus().send(msg);
}

void MediaPlayer2Player::currentMediaChanged(const media_t &media)
{
    m_metadata = media;

    propertiesChanged(u"Metadata"_s, Metadata());
    propertiesChanged(u"CanSeek"_s, CanSeek());
}

void MediaPlayer2Player::playerState(Player::PlayerState state)
{
    Q_UNUSED(state);
    propertiesChanged(u"PlaybackStatus"_s, PlaybackStatus());
}

void MediaPlayer2Player::seekableChanged(bool seekable)
{
    propertiesChanged(u"CanSeek"_s, seekable);
}

void MediaPlayer2Player::connectActions()
{
    connect(m_playlist, &PlaylistModel::currentMediaChanged, this, &MediaPlayer2Player::currentMediaChanged);
    connect(m_player, &Player::stateChanged, this, &MediaPlayer2Player::playerState);
    connect(m_player, &Player::seekableChanged, this, &MediaPlayer2Player::seekableChanged);

    connect(m_player, &Player::positionChanged, this, [this](qint64 pos) {
        emit Seeked(pos * 1000);
    });

}

bool MediaPlayer2Player::CanGoNext() const
{
    return true;
}

bool MediaPlayer2Player::CanGoPrevious() const
{
    return true;
}

bool MediaPlayer2Player::CanPlay() const
{
    return true;
}

bool MediaPlayer2Player::CanPause() const
{
    return true;
}
