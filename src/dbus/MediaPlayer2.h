#ifndef MEDIAPLAYER2_H
#define MEDIAPLAYER2_H

#include <QDBusAbstractAdaptor>
#include <QStringList>

class MediaPlayer2 : QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.mpris.MediaPlayer2");
    
    Q_PROPERTY(bool CanRaise READ CanRaise CONSTANT);
    Q_PROPERTY(bool CanQuit READ CanQuit CONSTANT);
    Q_PROPERTY(bool CanSetFullScreen READ CanSetFullScreen CONSTANT);
    Q_PROPERTY(bool FullScreen READ FullScreen CONSTANT);
    Q_PROPERTY(bool HasTrackList READ HasTrackList CONSTANT);
    Q_PROPERTY(QString Identity READ Identity CONSTANT);
    Q_PROPERTY(QString DesktopEntry READ DesktopEntry CONSTANT);
    Q_PROPERTY(QStringList SupportedUriSchemes READ SupportedUriSchemes CONSTANT);
    Q_PROPERTY(QStringList SupportedMimeTypes READ SupportedMimeTypes CONSTANT);
    
public:
    explicit MediaPlayer2(QObject *parent);
    ~MediaPlayer2();
    
    [[nodiscard]] bool CanRaise() const;
    [[nodiscard]] bool CanQuit() const;
    [[nodiscard]] bool CanSetFullScreen() const;
    [[nodiscard]] bool FullScreen() const;
    [[nodiscard]] bool HasTrackList() const;
    
    [[nodiscard]] QString Identity() const;
    [[nodiscard]] QString DesktopEntry() const;
    
    [[nodiscard]] QStringList SupportedUriSchemes() const;
    [[nodiscard]] QStringList SupportedMimeTypes() const;
    
public slots:
    Q_NOREPLY void Raise();
    Q_NOREPLY void Quit();

signals:
    void raisePlayer();
    void quitPlayer();
    
};

#endif //MEDIAPLAYER_H
