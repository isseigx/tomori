#ifndef TOMORI_H
#define TOMORI_H

#include <QObject>
#include <QPointer>

#include <Player.h>
#include <PlaylistModel.h>
#include <Song.h>
#include <Notification.h>
#include <DirScanner.h>

class Tomori : public QObject
{
    Q_OBJECT

public:
    explicit Tomori(QObject *parent = nullptr);
    ~Tomori();

    void createPlayer();
    void initDBus();
    void addFile(const QString &);
    void playFile(const QString &);
    bool testConnection();
    void readSettings();

    Player *currentPlayer() { return player; }
    PlaylistModel *currentPlaylist() { return playlistmodel; }

    void playPause();
    void next();
    void prev();
    void stop();
    
private slots:
    void onDBusActionInvoked(uint, const QString &);

private:
    void connectActions();
    void cacheLocation();
    void createDataLocation();

    QPointer<Player> player;
    Notification *noti;

    PlaylistModel *playlistmodel;

    const QString SERVICE = QStringLiteral("org.mpris.MediaPlayer2.ayase");
    const QString PATH = QStringLiteral("/org/mpris/MediaPlayer2");
    const QString INTERFACE = QStringLiteral("org.mpris.MediaPlayer2.Player");
};

#endif //TOMORI_H
