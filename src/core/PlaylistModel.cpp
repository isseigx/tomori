#include "PlaylistModel.h"

#include "Enums.h"

#include <algorithm>
#include <random>

#include <QCoroFuture>

#include "PlaylistParser.h"

PlaylistModel::PlaylistModel(QObject *parent) : QAbstractListModel(parent)
{

}

PlaylistModel::~PlaylistModel()
{

}

void PlaylistModel::addMedia(const media_t &media)
{
    addMedia(MediaList() << media);
}

void PlaylistModel::addMedia(const MediaList &media)
{
    auto pos = size();
    beginInsertRows({}, pos, pos + media.size() - 1);
    media_list.append(media);
    endInsertRows();

    for (auto&& m : media) {
        m_duration += m.length;
    }

    emit durationChanged(m_duration);
    emit sizeChanged(media_list.size());
}

QCoro::QmlTask PlaylistModel::addMedia(const QUrl &item)
{
    return addUrl(item);
}

QCoro::Task<> PlaylistModel::addDir(const QUrl &dir)
{
    const QList<QUrl> result = co_await DirScanner::scan(dir);

    if (result.isEmpty()) {
        co_return;
    }

    co_await addUrls(result);
}

QCoro::QmlTask PlaylistModel::addFolder(const QUrl &folder)
{
    return addDir(folder);
}

QCoro::QmlTask PlaylistModel::addMedia(const QList<QUrl> &items)
{
    return addUrls(items);
}

QCoro::Task<> PlaylistModel::addUrl(const QUrl &url)
{
    if (url.isLocalFile()) {
        const auto pos = size();
        addMedia({.path = url, .title = url.fileName()});

        QList<QUrl> urls = {url};

        const auto result = co_await TagReader::extractMetaData(std::move(urls));
        setData(index(pos, 0), QVariant::fromValue(result.at(0)));
    }
    else {
        addMedia({
            .path = url,
            .title = url.toString()
        });
    }
}

QCoro::Task<> PlaylistModel::addUrls(const QList<QUrl> &urls)
{
    MediaList list;
    list.reserve(urls.size());

    auto pos = size();

    for (const auto &url : urls) {
        list.append({.path = url, .title = url.fileName()});
    }

    addMedia(list);

    const auto result = co_await TagReader::extractMetaData(urls);

    for (const media_t &m : result) {
        setData(index(pos, 0), QVariant::fromValue(m));
        ++pos;
    }
}

void PlaylistModel::clear()
{
    remove(0, size());

    m_persistentIndex = QModelIndex();
    currentIndexChanged(-1);
}

int PlaylistModel::currentIndex() const
{
    return m_persistentIndex.row();
}

media_t PlaylistModel::currentMedia() const
{
    if (m_persistentIndex.isValid()) {
        return m_persistentIndex.data(Enums::All).value<media_t>();
    }
    else {
        return {};
    }
}

QVariant PlaylistModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() > media_list.size()) {
        return {};
    }

    const media_t &media = media_list.at(index.row());

    if (role == Enums::All) {
        return QVariant::fromValue(media);
    }
    else if (role == Enums::Title) {
        return media.title;
    }
    else if (role == Enums::Artist) {
        return media.artist;
    }
    else if (role == Enums::Album) {
        return media.album;
    }
    else if (role == Enums::AlbumArtist) {
        return media.albumArtist;
    }
    else if (role == Enums::Path) {
        return media.path;
    }
    else if (role == Enums::Image) {
        return media.image;
    }
    else if (role == Enums::Lyrics) {
        return media.lyrics;
    }
    else if (role == Enums::Year) {
        return media.year;
    }
    else if (role == Enums::Track) {
        return media.track;
    }
    else if (role == Enums::Length) {
        return media.length;
    }
    else if (role == Enums::Genre) {
        return media.genre;
    }
    else if (role == Enums::Comment) {
        return media.comment;
    }

    return {};
}

qint64 PlaylistModel::duration() const
{
    return m_duration;
}

void PlaylistModel::insert(int index, const QUrl &item)
{
    //TODO
    Q_UNUSED(index);
    Q_UNUSED(item);
}

void PlaylistModel::insert(int index, const QList<QUrl> &items)
{
    //TODO
    Q_UNUSED(index);
    Q_UNUSED(items);
}

void PlaylistModel::insert(int index, const media_t &media)
{
    index = qBound(0, index, media_list.size());
    beginInsertRows(QModelIndex(), index, index);
    media_list.insert(index, media);
    endInsertRows();

    m_duration += media.length;
    emit durationChanged(m_duration);
    emit sizeChanged(media_list.size());
}

void PlaylistModel::insert(int index, const MediaList &media)
{
    index = qBound(0, index, media_list.size());
    int last = index + media.size() - 1;
    beginInsertRows(QModelIndex(), index, last);

    auto list = media_list.mid(0, index);
    list += media;
    list += media_list.mid(index);
    media_list = list;

    endInsertRows();

    for (auto&& m : std::as_const(media)) {
        m_duration += m.length;
    }

    emit durationChanged(m_duration);
    emit sizeChanged(media_list.size());
}

void PlaylistModel::load()
{
    PlaylistParser parser;
    addMedia(parser.parse());
}

void PlaylistModel::load(const QUrl &file)
{
    PlaylistParser parser;
    addMedia(parser.parse(file.toLocalFile()));
}

void PlaylistModel::move(int from, int to)
{
    if (from < to) {
        moveRows(QModelIndex(), from, 1, QModelIndex(), to + 1);
    }
    else {
        moveRows(QModelIndex(), from, 1, QModelIndex(), to);
    }
}

bool PlaylistModel::moveRows(const QModelIndex &sourceParent, int sourceRow, int count, const QModelIndex &destinationParent, int destinationChild)
{
    if (sourceParent != destinationParent) {
        return false;
    }

    if (!beginMoveRows(sourceParent, sourceRow, sourceRow + count -1, destinationParent, destinationChild)) {
        return false;
    }

    for (auto i = 0; i < count; ++i) {
        if (sourceRow < destinationChild) {
            media_list.move(sourceRow, destinationChild - 1);
        }
        else {
            media_list.move(sourceRow, destinationChild);
        }
    }

    endMoveRows();

    return true;
}

void PlaylistModel::next()
{
    if (size() == 0) {
        return;
    }

    m_persistentIndex = index(m_persistentIndex.row() + 1);

    emit currentIndexChanged(m_persistentIndex.row());

    if (m_persistentIndex.isValid()) {
        emit currentMediaChanged(currentMedia());
    }
}

void PlaylistModel::previous()
{
    if (size() == 0) {
        return;
    }

    m_persistentIndex = index(m_persistentIndex.row() - 1);

    if (!m_persistentIndex.isValid()) {
        m_persistentIndex = index(size() - 1);
    }

    emit currentIndexChanged(m_persistentIndex.row());
    emit currentMediaChanged(currentMedia());
}

void PlaylistModel::remove(int index)
{
    removeRows(index, 1);
}

void PlaylistModel::remove(int start, int end)
{
    if (end < start || end < 0 || start >= media_list.count()) {
        return;
    }

    removeRows(start, end);
}

bool PlaylistModel::removeRows(int row, int count, const QModelIndex &parent)
{
    if (count <= 0 || row < 0 || (row + count) > rowCount(parent)) {
        return false;
    }

    beginRemoveRows(parent, row, row + count - 1);

    const auto it = media_list.begin() + row;
    media_list.erase(it, it + count);

    endRemoveRows();

    m_duration = 0;

    for (const media_t &m : media_list) {
        m_duration += m.length;
    }

    emit durationChanged(m_duration);
    emit sizeChanged(media_list.size());

    return true;
}

void PlaylistModel::restore()
{
    PlaylistParser parser;
    addMedia(parser.parse());
}

int PlaylistModel::rowCount(const QModelIndex &index) const
{
    Q_UNUSED(index);
    return media_list.count();
}

QHash<int, QByteArray> PlaylistModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[Enums::All] = QByteArrayLiteral("media");
    roles[Enums::Title] = QByteArrayLiteral("title");
    roles[Enums::Artist] = QByteArrayLiteral("artist");
    roles[Enums::Album] = QByteArrayLiteral("album");
    roles[Enums::AlbumArtist] = QByteArrayLiteral("albumArtist");
    roles[Enums::Path] = QByteArrayLiteral("path");
    roles[Enums::Image] = QByteArrayLiteral("image");
    roles[Enums::Lyrics] = QByteArrayLiteral("lyrics");
    roles[Enums::Year] = QByteArrayLiteral("year");
    roles[Enums::Track] = QByteArrayLiteral("track");
    roles[Enums::Length] = QByteArrayLiteral("length");
    roles[Enums::Genre] = QByteArrayLiteral("genre");
    roles[Enums::Comment] = QByteArrayLiteral("comment");

    return roles;
}

void PlaylistModel::save()
{
    PlaylistParser parser;
    parser.savePlaylist(media_list);
}

void PlaylistModel::save(const QUrl &file)
{
    PlaylistParser parser;
    parser.savePlaylist(file.toLocalFile(), media_list);
}

void PlaylistModel::setCurrentIndex(int idx)
{
    if (idx < 0 || idx >= media_list.size()) {
        return;
    }

    m_persistentIndex = index(idx);

    emit currentIndexChanged(currentIndex());
    emit currentMediaChanged(currentMedia());
}

bool PlaylistModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    const auto media = value.value<media_t>();

    if (index.row() >= 0 && index.row() < size()) {
        m_duration = m_duration - media_list.at(index.row()).length + media.length;
        media_list.replace(index.row(), media);
        dataChanged(index, index, {
            Enums::All,
            Enums::Title,
            Enums::Artist,
            Enums::Album,
            Enums::AlbumArtist,
            Enums::Path,
            Enums::Image,
            Enums::Lyrics,
            Enums::Year,
            Enums::Length,
            Enums::Genre,
            Enums::Comment
        });
        emit durationChanged(m_duration);
        return true;
    }
    return false;
}

qsizetype PlaylistModel::size() const
{
    return media_list.size();
}

void PlaylistModel::shuffle()
{
    std::random_device rnd;
    std::mt19937 mt(rnd());

    beginResetModel();
    std::shuffle(media_list.begin(), media_list.end(), mt);
    endResetModel();
}
