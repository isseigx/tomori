#include "Library.h"

#include <QDateTime>
#include <QDebug>
#include <QFileInfo>
#include <QSettings>
#include <QtConcurrent>

#include <QCoroFuture>

#include "MusicDatabase.h"
#include "Utils.h"

UpdateResult updateLibrary(const QList<QUrl> &diskFiles, const QList<QUrl> &dbFiles)
{
    QSettings settings;
    const QDateTime updateDateTime = settings.value(QLatin1String("LibraryLastUpdate"),
                                            QDateTime::currentDateTime()).toDateTime();

    QList<QUrl> deletedFiles, addedFiles;

    for (auto&& file : dbFiles) {
        if (!diskFiles.contains(file)) {
            deletedFiles << file;
        }
    }

    for (auto&& file : diskFiles) {
        if (!dbFiles.contains(file)) {
            addedFiles << file;
            continue;
        }

        if (QFileInfo(file.toLocalFile()).lastModified() > updateDateTime) {
            addedFiles << file;
        }
    }

    return {addedFiles, deletedFiles};
}

Library::Library(QObject *parent) : QObject(parent)
{
}

Library::~Library()
{
}

QCoro::QmlTask Library::update()
{
    return startUpdate();
}

void Library::forceUpdate()
{
    MusicDatabase::instance().deleteAll();
    update();
}

MediaList Library::getSongs(Enums::MetaDataField field, const QVariant &value) const
{
    QString fieldString;

    switch (field) {
        case Enums::Album:
            fieldString = QStringLiteral("album");
            break;
        case Enums::Artist:
            fieldString = QStringLiteral("artist");
            break;
        case Enums::Genre:
            fieldString = QStringLiteral("genre");
            break;
        case Enums::Year:
            fieldString = QStringLiteral("year");
            break;
        default:
            return MusicDatabase::instance().getAllSongs();
    }

    return MusicDatabase::instance().getSongs(fieldString, value);
}

MediaList Library::getAlbums(const QString &artist) const
{
    if (artist.isEmpty()) {
        return MusicDatabase::instance().getAlbums();
    }
    else {
        return MusicDatabase::instance().getAlbums(artist);
    }
}

MediaList Library::getArtists() const
{
    return MusicDatabase::instance().getArtists();
}

QCoro::Task<> Library::startUpdate()
{
    emit updateStarted();

    const auto diskFiles = co_await DirScanner::scan(QUrl::fromLocalFile(Utils::instance().musicDir()));

    const auto dbFiles = MusicDatabase::instance().getFiles();

    const UpdateResult res = co_await QtConcurrent::run(updateLibrary, diskFiles, dbFiles);

    MusicDatabase::instance().removeSongs(res.deletedFiles);

    const auto songs = co_await TagReader::extractMetaData(res.addedFiles);

    MusicDatabase::instance().addSongs(songs);

    emit updateFinished();
}
