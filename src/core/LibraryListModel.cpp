#include "LibraryListModel.h"

#include <Enums.h>

LibraryListModel::LibraryListModel(QObject *parent) : QAbstractListModel(parent)
{
}

LibraryListModel::~LibraryListModel()
{
}

QVariant LibraryListModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() > m_mediaList.size()) {
        return {};
    }

    const media_t &s = m_mediaList.at(index.row());

    if (role == Enums::All)
        return QVariant::fromValue(s);
    if (role == Enums::Title)
        return s.title;
    else if (role == Enums::Artist)
        return s.artist;
    else if (role == Enums::Album)
        return s.album;
    else if (role == Enums::AlbumArtist)
        return s.albumArtist;
    else if (role == Enums::Path)
        return s.path;
    else if (role == Enums::Image)
        return s.image;
    else if (role == Enums::Lyrics)
        return s.lyrics;
    else if (role == Enums::Year)
        return s.year;
    else if (role == Enums::Track)
        return s.track;
    else if (role == Enums::Length)
        return s.length;
    else if (role == Enums::Genre)
        return s.genre;
    else if (role == Enums::Comment)
        return s.comment;

    return {};
}

bool LibraryListModel::moveRows(const QModelIndex &sourceParent, int sourceRow, int count, const QModelIndex &destinationParent, int destinationChild)
{
    if (sourceParent != destinationParent) {
        return false;
    }

    if (!beginMoveRows(sourceParent, sourceRow, sourceRow + count - 1, destinationParent, destinationChild)) {
        return false;
    }

    for (int i = 0; i < count; ++i) {
        if (sourceRow < destinationChild) {
            m_mediaList.move(sourceRow, destinationChild - 1);
        } else {
            m_mediaList.move(sourceRow, destinationChild);
        }
    }

    endMoveRows();

    return true;
}

bool LibraryListModel::removeRows(int row, int count, const QModelIndex &parent)
{
    if (count <= 0 || row < 0 || (row + count) > rowCount(parent)) {
        return false;
    }

    beginRemoveRows(parent, row, row + count - 1);

    const auto it = m_mediaList.begin() + row;
    m_mediaList.erase(it, it + count);

    endRemoveRows();

    emit countChanged(m_mediaList.size());

    emit mediaListChanged();
    return true;
}

int LibraryListModel::rowCount(const QModelIndex &index) const
{
    return index.isValid() ? 0 : m_mediaList.size();
}

QHash<int, QByteArray> LibraryListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[Enums::All] = QByteArrayLiteral("media");
    roles[Enums::Title] = QByteArrayLiteral("title");
    roles[Enums::Artist] = QByteArrayLiteral("artist");
    roles[Enums::Album] = QByteArrayLiteral("album");
    roles[Enums::AlbumArtist] = QByteArrayLiteral("albumArtist");
    roles[Enums::Path] = QByteArrayLiteral("path");
    roles[Enums::Image] = QByteArrayLiteral("image");
    roles[Enums::Lyrics] = QByteArrayLiteral("lyrics");
    roles[Enums::Year] = QByteArrayLiteral("year");
    roles[Enums::Track] = QByteArrayLiteral("track");
    roles[Enums::Length] = QByteArrayLiteral("length");
    roles[Enums::Genre] = QByteArrayLiteral("genre");
    roles[Enums::Comment] = QByteArrayLiteral("comment");

    return roles;
}

void LibraryListModel::setMediaList(const MediaList &songs)
{
    beginResetModel();
    m_mediaList = songs;
    endResetModel();

    emit countChanged(m_mediaList.size());
    emit mediaListChanged();
}

media_t LibraryListModel::mediaAt(int index) const
{
    if (m_mediaList.isEmpty()) {
        return {};
    }

    return m_mediaList.at(index);
}

void LibraryListModel::add(const MediaList &list)
{
    setMediaList(list);
}

void LibraryListModel::append(const MediaList &list)
{
    beginInsertRows({}, m_mediaList.size(), m_mediaList.size() + list.size() - 1);
    m_mediaList << list;
    endInsertRows();

    emit countChanged(m_mediaList.size());
    emit mediaListChanged();
}

int LibraryListModel::count() const
{
    return m_mediaList.size();
}
