#pragma once

#include <QMetaType>
#include <QObject>
#include <QString>
#include <QtQml/qqmlregistration.h>
#include <QUrl>
#include <QVariant>

struct media_t
{
    Q_GADGET
    QML_NAMED_ELEMENT(media)
    QML_UNCREATABLE("")

    Q_PROPERTY(QUrl path MEMBER path)
    Q_PROPERTY(QString title MEMBER title)
    Q_PROPERTY(QString artist MEMBER artist)
    Q_PROPERTY(QString albumArtist MEMBER albumArtist)
    Q_PROPERTY(QString album MEMBER album)
    Q_PROPERTY(QUrl image MEMBER image)
    Q_PROPERTY(QString lyrics MEMBER lyrics)
    Q_PROPERTY(uint year MEMBER year)
    Q_PROPERTY(uint track MEMBER track)
    Q_PROPERTY(int length MEMBER length)
    Q_PROPERTY(QString genre MEMBER genre)
    Q_PROPERTY(QString comment MEMBER comment)
    Q_PROPERTY(QVariantMap audioProperties MEMBER audioProperties)
public:
    QUrl path;
    QString title;
    QString artist;
    QString album;
    QString albumArtist;
    QUrl image;
    QString lyrics;
    uint year;
    uint track;
    int length;
    QString genre;
    QString comment;
    QVariantMap audioProperties;
};

typedef QList<media_t> MediaList;

Q_DECLARE_METATYPE(MediaList)
