#ifndef LIBRARY_H
#define LIBRARY_H

#include <QFutureWatcher>
#include <QObject>
#include <QQmlEngine>
#include <QUrl>
#include <QVariant>

#include <QCoroQmlTask>

#include "Enums.h"
#include "DirScanner.h"
#include "Media.h"
#include "Tags.h"

struct UpdateResult {
    QList<QUrl> addedFiles;
    QList<QUrl> deletedFiles;
};

class Library : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_SINGLETON

public:
    static Library &instance()
    {
        static Library _instance;
        return _instance;
    }

    static Library *create(QQmlEngine *engine, QJSEngine *)
    {
        engine->setObjectOwnership(&instance(), QQmlEngine::CppOwnership);
        return &instance();
    }

    ~Library();

    Q_INVOKABLE QCoro::QmlTask update();

    Q_INVOKABLE void forceUpdate();

    Q_INVOKABLE MediaList getSongs(Enums::MetaDataField = Enums::All, const QVariant & = QVariant()) const;

    Q_INVOKABLE MediaList getAlbums(const QString & = QString()) const;

    Q_INVOKABLE MediaList getArtists() const;

signals:
    void updateStarted();
    void updateFinished();

private:
    explicit Library(QObject *parent = nullptr);

    QCoro::Task<> startUpdate();
};

#endif //LIBRARY_H
