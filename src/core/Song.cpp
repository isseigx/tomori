#include "Song.h"

#include <QFileInfo>

Song::Song(const QString &title, const QString &artist, const QString &album, const QString &path) :
title_g(title), artist_g(artist), album_g(album), path_g(path)
{
    filename_g = QFileInfo(path).fileName();
}

Song::Song(const QString &path) : path_g(path)
{
    filename_g = QFileInfo(path).fileName();
}

Song& Song::operator=(const Song &song)
{
    title_g = song.title();
    artist_g = song.artist();
    album_g = song.album();
    path_g = song.path();
    imagePath_g = song.imagePath();
    lyrics_g = song.lyrics();
    year_g = song.year();
    return *this;
}
