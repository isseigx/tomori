#ifndef PLAYER_H
#define PLAYER_H

#include <QObject>
#include <QUrl>
#include <QtQml/qqmlregistration.h>

#include <vlc/vlc.h>

class Player : public QObject
{
    Q_OBJECT
    QML_ELEMENT

    Q_PROPERTY(qint64 duration READ duration NOTIFY durationChanged)
    Q_PROPERTY(QString engineVersion READ engineVersion CONSTANT)
    Q_PROPERTY(QUrl source READ source WRITE setSource NOTIFY sourceChanged)
    Q_PROPERTY(MediaStatus mediaStatus READ mediaStatus NOTIFY mediaStatusChanged)
    Q_PROPERTY(qint64 position READ position WRITE setPosition NOTIFY positionChanged)
    Q_PROPERTY(bool seekable READ seekable NOTIFY seekableChanged)
    Q_PROPERTY(PlayerState state READ state NOTIFY stateChanged)


public:
    explicit Player(QObject *parent = nullptr);
    ~Player();

    enum PlayerState {
        Playing,
        Paused,
        Stopped
    };

    Q_ENUM(PlayerState)

    enum MediaStatus {
        None,
        Opening,
        Buffering,
        EndReached,
        Error
    };

    Q_ENUM(MediaStatus)

    qint64 duration() const;

    QString engineVersion() const;

    void eventTriggered(const struct libvlc_event_t *event);

    MediaStatus mediaStatus() const;

    qint64 position() const;

    bool seekable() const;

    PlayerState state() const;

    QUrl source() const;

public slots:
    void play();
    void setPosition(qint64 pos);
    void setSource(const QUrl &path);
    void stop();

signals:
    void durationChanged(qint64 duration);
    void mediaStatusChanged(MediaStatus status);
    void positionChanged(qint64 position);
    void seekableChanged(bool seekable);
    void sourceChanged(const QUrl &source);
    void stateChanged(PlayerState state);

private:
    void attachToEvents();
    void onCurrentMediaChanged(struct libvlc_media_t *media);
    void onMediaStatusChanged(MediaStatus status);
    void onPlayerStateChanged(PlayerState state);

    qint64 m_duration = 0, m_position = 0;

    MediaStatus m_mediaStatus = Player::None;
    PlayerState m_playerState = Player::Stopped;

    bool m_seekable = false;
    QUrl m_source;

    libvlc_instance_t *m_vlc;
    libvlc_media_player_t *m_vlcPlayer;
};

#endif //PLAYER_H
