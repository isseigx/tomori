#include "Player.h"

#include <vlc/libvlc_version.h>

static inline void event_callback(const struct libvlc_event_t *event, void *data)
{
    reinterpret_cast<Player *>(data)->eventTriggered(event);
}

Player::Player(QObject *parent) : QObject(parent)
{
    m_vlc = libvlc_new(0, nullptr);

    if (!m_vlc) {
        qFatal("[Player]: failed to create libvlc instance");
        return;
    }

    libvlc_set_app_id(m_vlc, "io.gitlab.isseigx.ayase", AYASE_VERSION, "ayase");

    libvlc_set_user_agent(m_vlc, "Ayase", AYASE_AGENT);

    m_vlcPlayer = libvlc_media_player_new(m_vlc);

    libvlc_media_player_set_role(m_vlcPlayer, libvlc_role_Music);

    attachToEvents();
}

Player::~Player()
{
    if (m_vlcPlayer) {
        libvlc_media_player_release(m_vlcPlayer);
    }

    if (m_vlc) {
        libvlc_release(m_vlc);
    }
}

void Player::attachToEvents()
{
    auto manager = libvlc_media_player_event_manager(m_vlcPlayer);

    libvlc_event_attach(manager, libvlc_MediaPlayerOpening, &event_callback, this);

    libvlc_event_attach(manager, libvlc_MediaPlayerBuffering, &event_callback, this);

    libvlc_event_attach(manager, libvlc_MediaPlayerPlaying, &event_callback, this);

    libvlc_event_attach(manager, libvlc_MediaPlayerPaused, &event_callback, this);

    libvlc_event_attach(manager, libvlc_MediaPlayerStopped, &event_callback, this);

    libvlc_event_attach(manager, libvlc_MediaPlayerEndReached, &event_callback, this);

    libvlc_event_attach(manager, libvlc_MediaPlayerEncounteredError, &event_callback, this);

    libvlc_event_attach(manager, libvlc_MediaPlayerMediaChanged, &event_callback, this);

    libvlc_event_attach(manager, libvlc_MediaPlayerTimeChanged, &event_callback, this);

    libvlc_event_attach(manager, libvlc_MediaPlayerSeekableChanged, &event_callback, this);

    libvlc_event_attach(manager, libvlc_MediaPlayerLengthChanged, &event_callback, this);
}

qint64 Player::duration() const
{
    return m_duration;
}

QString Player::engineVersion() const
{
    return QString::fromUtf8(libvlc_get_version());
}

void Player::eventTriggered(const struct libvlc_event_t *event)
{
    switch (event->type) {
        case libvlc_MediaPlayerMediaChanged:
            onCurrentMediaChanged(event->u.media_player_media_changed.new_media);
            break;
        case libvlc_MediaPlayerOpening:
            onMediaStatusChanged(Player::Opening);
            break;
        case libvlc_MediaPlayerBuffering:
            onMediaStatusChanged(Player::Buffering);
            break;
        case libvlc_MediaPlayerPlaying:
            onPlayerStateChanged(Player::Playing);
            break;
        case libvlc_MediaPlayerPaused:
            onPlayerStateChanged(Player::Paused);
            break;
        case libvlc_MediaPlayerStopped:
            onPlayerStateChanged(Player::Stopped);
            break;
        case libvlc_MediaPlayerEndReached:
            onMediaStatusChanged(Player::EndReached);
            break;
        case libvlc_MediaPlayerEncounteredError:
            onMediaStatusChanged(Player::Error);
            break;
        case libvlc_MediaPlayerTimeChanged: {
            m_position = event->u.media_player_time_changed.new_time;
            QMetaObject::invokeMethod(this, "positionChanged", Qt::QueuedConnection, m_position);
            break;
        }
        case libvlc_MediaPlayerSeekableChanged: {
            m_seekable = event->u.media_player_seekable_changed.new_seekable;
            QMetaObject::invokeMethod(this, "seekableChanged", Qt::QueuedConnection, m_seekable);
            break;
        }
        case libvlc_MediaPlayerLengthChanged: {
            m_duration = event->u.media_player_length_changed.new_length;
            QMetaObject::invokeMethod(this, "durationChanged", Qt::QueuedConnection, m_duration);
            break;
        }
        default:
            break;
    }
}

void Player::onCurrentMediaChanged(struct libvlc_media_t *media)
{
    m_source = {QUrl::fromPercentEncoding(libvlc_media_get_mrl(media))};

    QMetaObject::invokeMethod(this, "sourceChanged", Qt::QueuedConnection, m_source);
}

void Player::onMediaStatusChanged(MediaStatus status)
{
    if (status != m_mediaStatus) {
        m_mediaStatus = status;
        QMetaObject::invokeMethod(this, "mediaStatusChanged", Qt::QueuedConnection, m_mediaStatus);
    }
}

void Player::onPlayerStateChanged(PlayerState state)
{
    if (state != m_playerState) {
        m_playerState = state;
        QMetaObject::invokeMethod(this, "stateChanged", Qt::QueuedConnection, m_playerState);
    }
}

Player::MediaStatus Player::mediaStatus() const
{
    return m_mediaStatus;
}

void Player::play()
{
    if (m_playerState == Player::Playing) {
        libvlc_media_player_pause(m_vlcPlayer);
    }
    else {
        libvlc_media_player_play(m_vlcPlayer);
    }
}

qint64 Player::position() const
{
    return m_position;
}

bool Player::seekable() const
{
    return m_seekable;
}

void Player::setPosition(qint64 position)
{
    if (m_seekable) {
        libvlc_media_player_set_time(m_vlcPlayer, (libvlc_time_t)position);
    }
}

void Player::setSource(const QUrl &source)
{

    if (source.isLocalFile()) {
        libvlc_media_player_set_media(
            m_vlcPlayer,
            libvlc_media_new_path(
                m_vlc,
                source.toLocalFile().toUtf8().constData()
            )
        );
    }
    else {
        libvlc_media_player_set_media(
            m_vlcPlayer,
            libvlc_media_new_location(
                m_vlc,
                source.toString().toUtf8().constData()
            )
        );
    }
}

QUrl Player::source() const
{
    return m_source;
}

Player::PlayerState Player::state() const
{
    return m_playerState;
}

void Player::stop()
{
    #if LIBVLC_VERSION_INT >= LIBVLC_VERSION(4, 0, 0, 0)
    libvlc_media_player_stop_async(m_vlcPlayer);
    #else
    libvlc_media_player_stop(m_vlcPlayer);
    #endif
}
