#include "Utils.h"

#include <QFileInfo>
#include <QStandardPaths>
#include <QTime>

#include "MimeChecker.h"

Utils::Utils(QObject *parent) : QObject(parent)
{
    m_currentdir = QUrl::fromLocalFile(musicDir());
}

Utils::~Utils()
{
}

QString Utils::cacheDir() const
{
    const auto dir = QStandardPaths::standardLocations(QStandardPaths::CacheLocation)
                                        .constFirst();
    return QStringLiteral("%1/").arg(dir);
}

QString Utils::musicDir() const
{
    const auto dir = QStandardPaths::standardLocations(QStandardPaths::MusicLocation)
                                        .constFirst();
    return dir;
}

void Utils::setCurrentDir(const QUrl &dir)
{
    m_currentdir = dir;
    emit currentDirChanged(m_currentdir);
}

QString Utils::timeToString(qint64 msecs) const
{
    QString format;

    if (msecs > 3600000) {
        format = QStringLiteral("h:mm:ss");
    }
    else {
        format = QStringLiteral("mm:ss");
    }

    return QTime::fromMSecsSinceStartOfDay(msecs).toString(format);
}

QList<QUrl> Utils::checkFiles(const QList<QUrl> &files) const
{
    QList<QUrl> supportedFiles;

    for (auto&& file : files) {
        const QString tmp = file.toLocalFile(); // convert url to local file
        // if the path is a directory, ignore it
        if (!QFileInfo(tmp).isDir()) {
            // add path only if is supported by player
            if (MimeChecker::isAudioFileSupported(tmp)) {
                supportedFiles.append(file);
            }
        }
    }

    return supportedFiles;
}
