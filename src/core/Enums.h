#pragma once

#include <QObject>
#include <QtQml/qqmlregistration.h>

namespace Enums {

Q_NAMESPACE
QML_ELEMENT

enum MetaDataField {
    All = Qt::UserRole + 1,
    Title,
    Artist,
    Album,
    AlbumArtist,
    Path,
    Image,
    Lyrics,
    Year,
    Track,
    Length,
    Genre,
    Comment
};

Q_ENUM_NS(MetaDataField)


enum QueryType {
    Connection,
    Create,
    Delete,
    Drop,
    Insert,
    Select,
    Update
};

Q_ENUM_NS(QueryType)

}
