#ifndef PLAYLISTMODEL_H
#define PLAYLISTMODEL_H

#include <QAbstractListModel>
#include <QtQml/qqmlregistration.h>

#include <QCoroQmlTask>

#include "DirScanner.h"
#include "Media.h"
#include "Tags.h"

class PlaylistModel : public QAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT

    Q_PROPERTY(int currentIndex READ currentIndex WRITE setCurrentIndex NOTIFY currentIndexChanged)
    Q_PROPERTY(qint64 duration READ duration NOTIFY durationChanged)
    Q_PROPERTY(qsizetype size READ size NOTIFY sizeChanged)
    Q_PROPERTY(media_t currentMedia READ currentMedia NOTIFY currentMediaChanged)


public:
    explicit PlaylistModel(QObject *parent = nullptr);
    ~PlaylistModel();

    QVariant data(const QModelIndex &, int = Qt::DisplayRole) const override;

    bool moveRows(const QModelIndex &, int, int, const QModelIndex &, int) override;

    bool setData(const QModelIndex &, const QVariant &, int = Qt::EditRole) override;

    bool removeRows(int, int, const QModelIndex & = QModelIndex()) override;

    QHash<int, QByteArray> roleNames() const override;

    int rowCount(const QModelIndex &) const override;

    int currentIndex() const;

    media_t currentMedia() const;

    qint64 duration() const;

    Q_INVOKABLE QCoro::QmlTask addFolder(const QUrl &folder);

    Q_INVOKABLE QCoro::QmlTask addMedia(const QUrl &item);
    Q_INVOKABLE QCoro::QmlTask addMedia(const QList<QUrl> &items);

    Q_INVOKABLE void insert(int index, const QUrl &item);
    Q_INVOKABLE void insert(int index, const QList<QUrl> &items);
    Q_INVOKABLE void insert(int index, const media_t &media);
    Q_INVOKABLE void insert(int index, const MediaList &media);

    Q_INVOKABLE void move(int from, int to);
    Q_INVOKABLE void remove(int index);
    Q_INVOKABLE void remove(int start, int end);

    Q_INVOKABLE void load(const QUrl &file);
    Q_INVOKABLE void save(const QUrl &file);

    qsizetype size() const;

public slots:
    void addMedia(const media_t &media);
    void addMedia(const MediaList &media);
    void clear();
    void load();
    void next();
    void previous();
    void restore();
    void save();
    void setCurrentIndex(int idx);
    void shuffle();

signals:
    void currentIndexChanged(int index);
    void currentMediaChanged(const media_t &media);
    void durationChanged(qint64 duration);
    void sizeChanged(qsizetype size);

private:
    QCoro::Task<> addDir(const QUrl &dir);
    QCoro::Task<> addUrl(const QUrl &url);
    QCoro::Task<> addUrls(const QList<QUrl> &urls);

    qint64 m_duration = 0;
    QPersistentModelIndex m_persistentIndex;
    MediaList media_list;
};

#endif //PLAYLISTMODEL_H
