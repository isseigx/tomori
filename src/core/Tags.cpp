#include "Tags.h"

#include <QDir>
#include <QFile>
#include <QImage>
#include <QMimeType>
#include <QMimeDatabase>
#include <QRegularExpression>
#include <QStandardPaths>
#include <QtConcurrent>

#include <cstdlib>
#include <taglib/attachedpictureframe.h>
#include <taglib/flacfile.h>
#include <taglib/flacpicture.h>
#include <taglib/id3v2tag.h>
#include <taglib/mp4coverart.h>
#include <taglib/mp4file.h>
#include <taglib/mp4tag.h>
#include <taglib/mpegfile.h>
#include <taglib/opusfile.h>
#include <taglib/tlist.h>
#include <taglib/tpropertymap.h>
#include <taglib/vorbisfile.h>
#include <taglib/xiphcomment.h>
#include <taglib/unsynchronizedlyricsframe.h>

#include "DirScanner.h"
#include "Utils.h"

#define TOQSTRING(s) QString::fromStdString(s.to8Bit(true))

using namespace Qt::Literals::StringLiterals;

static inline void checkEmptyValues(media_t &m)
{
    if (m.artist.isEmpty())
        m.artist = QObject::tr("<Unknown Artist>");

    if (m.album.isEmpty())
        m.album = QObject::tr("<Unknown Album>");

    if (m.title.isEmpty())
        m.title = m.path.fileName();
}

static inline QString saveImage(const QImage &img, const QString &filename)
{
    if (QFile::exists(filename)) {
        return filename;
    }

    if (img.save(filename, "JPG")) {
        return filename;
    }

    return QString();
}

static inline QString buildImageName(const media_t &media)
{
    QString artist = media.artist;
    QString album = media.album;

    const auto dataDir = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation).constFirst();
    const auto path = u"%1/covers/%2"_s.arg(
        dataDir,
        artist.replace(QRegularExpression(R"(\"|@|&|\'|\(|\)|<|>|#|\/|\\)"), "")
            % QDir::separator()
            % album.replace(QRegularExpression(R"(\"|@|&|\'|\(|\)|<|>|#|\/|\\)"), "")
    );

    QDir dir(dataDir);
    dir.mkpath(path);

    return u"%1.jpg"_s.arg(path % QDir::separator() % album);
}

static inline void extractMpegImage(TagLib::ID3v2::Tag *id3, media_t &media)
{
    const auto framelist = id3->frameList("APIC");
    QImage img;

    if (framelist.isEmpty()) {
        const auto imgUrl = QUrl::fromLocalFile(DirScan::getImageForFile(media.path.toLocalFile()));
        media.image = imgUrl;
        return;
    }
    
    auto pix = static_cast<TagLib::ID3v2::AttachedPictureFrame *>(framelist.front());

    img.loadFromData((const uchar *)pix->picture().data(), pix->picture().size());
    const QString filepath = saveImage(img, buildImageName(media));

    media.image = QUrl::fromLocalFile(filepath);

}

static inline void extractMp4Image(const TagLib::MP4::ItemMap &itemMap, media_t &media)
{
    const auto coverlist = itemMap["covr"].toCoverArtList();
    QImage img;
    
    if (coverlist.isEmpty()) {
        const auto imgUrl = QUrl::fromLocalFile(DirScan::getImageForFile(media.path.toLocalFile()));
        media.image = imgUrl;
        return;
    }
    
    img.loadFromData((const uchar *)coverlist.front().data().data(), 
                     coverlist.front().data().size());
    const QString filepath = saveImage(img, buildImageName(media));
    media.image = QUrl::fromLocalFile(filepath);
}

static inline void extractFlacImage(const TagLib::List<TagLib::FLAC::Picture *> &piclist, media_t &media)
{
    QImage img;
    
    if (piclist.isEmpty()) {
        const auto imgUrl = QUrl::fromLocalFile(DirScan::getImageForFile(media.path.toLocalFile()));
        media.image = imgUrl;
        return;
    }
    
    img.loadFromData((const uchar *)piclist.front()->data().data(),
            piclist.front()->data().size());
    
    const QString filepath = saveImage(img, buildImageName(media));
    media.image = QUrl::fromLocalFile(filepath);
}

static void extractID3v2(TagLib::ID3v2::Tag *id3, media_t &media)
{
    media.title = TOQSTRING(id3->title());
    media.artist = TOQSTRING(id3->artist());
    media.album = TOQSTRING(id3->album());
    media.year = id3->year();
    media.track = id3->track();
    media.comment = TOQSTRING(id3->comment());
    media.genre = TOQSTRING(id3->genre());
    checkEmptyValues(media);

    const auto props = id3->properties();
    if (props.contains("ALBUMARTIST")) {
        media.albumArtist = TOQSTRING(props["ALBUMARTIST"].toString("; "));
    }

    auto frameList = id3->frameListMap()["USLT"];

    if (frameList.isEmpty()) {
        return;
    }

    auto frame = static_cast<TagLib::ID3v2::UnsynchronizedLyricsFrame *>(frameList.front());

    media.lyrics = TOQSTRING(frame->text());
}

static inline void extractXiphComment(TagLib::Ogg::XiphComment *xiph, media_t &media)
{
    media.title = TOQSTRING(xiph->title());
    media.artist = TOQSTRING(xiph->artist());
    media.album = TOQSTRING(xiph->album());
    media.year = xiph->year();
    media.track = xiph->track();
    media.comment = TOQSTRING(xiph->comment());
    media.genre = TOQSTRING(xiph->genre());

    checkEmptyValues(media);

    const auto props = xiph->properties();
    if (props.contains("ALBUMARTIST")) {
        media.albumArtist = TOQSTRING(props["ALBUMARTIST"].toString("; "));
    }

    if (!xiph->contains("LYRICS")) {
        return;
    }

    auto fieldlist = xiph->fieldListMap()["LYRICS"];
    if (!fieldlist.isEmpty()) {
        media.lyrics = TOQSTRING(fieldlist.front());
    }
}

static inline media_t extractFromMpeg(const QString &file)
{
    TagLib::MPEG::File info(file.toUtf8().constData());
    auto *id3 = info.ID3v2Tag(true);
    
    media_t media { .path = QUrl::fromLocalFile(file) };
    extractID3v2(id3, media);
    
    if (info.audioProperties()) {
        media.length = info.audioProperties()->lengthInMilliseconds();
        media.audioProperties = {
            {u"bitrate"_s, info.audioProperties()->bitrate()},
            {u"sampleRate"_s, info.audioProperties()->sampleRate()},
            {u"channels"_s, info.audioProperties()->channels()}
        };
    }

    extractMpegImage(id3, media);

    return media;
}

static media_t extractFromMp4(const QString &file)
{
    TagLib::MP4::File info(file.toUtf8().constData());

    if (!info.hasMP4Tag()) {
        media_t s = { .path = QUrl::fromLocalFile(file) };
        checkEmptyValues(s);
        return s;
    }

    media_t media {
        .path = QUrl::fromLocalFile(file),
        .title = TOQSTRING(info.tag()->title()),
        .artist = TOQSTRING(info.tag()->artist()),
        .album = TOQSTRING(info.tag()->album()),
        .year = info.tag()->year(),
        .track = info.tag()->track(),
        .genre = TOQSTRING(info.tag()->genre()),
        .comment = TOQSTRING(info.tag()->comment()),
    };

    auto props = info.tag()->properties();
    if (props.contains("ALBUMARTIST")) {
        media.albumArtist = TOQSTRING(props["ALBUMARTIST"].toString("; "));
    }
    
    if (info.audioProperties()) {
        media.length = info.audioProperties()->lengthInMilliseconds();
        media.audioProperties = {
            {u"bitrate"_s, info.audioProperties()->bitrate()},
            {u"sampleRate"_s, info.audioProperties()->sampleRate()},
            {u"channels"_s, info.audioProperties()->channels()},
            {u"bitsPerSample"_s, info.audioProperties()->bitsPerSample()}
        };
    }

    auto item = info.tag()->itemMap()["\xa9lyr"];
    auto stringList = item.toStringList();

    if (!stringList.isEmpty()) {
        media.lyrics = TOQSTRING(stringList.front());
    }

    extractMp4Image(info.tag()->itemMap(), media);

    checkEmptyValues(media);
    return media;
}

static inline media_t extractFromOgg(const QString &file)
{
    TagLib::Ogg::Vorbis::File info(file.toUtf8().constData());

    media_t media { .path = QUrl::fromLocalFile(file) };
    extractXiphComment(info.tag(), media);
    
    if (info.audioProperties()) {
        media.length = info.audioProperties()->lengthInMilliseconds();
        media.audioProperties = {
            {u"bitrate"_s, info.audioProperties()->bitrate()},
            {u"sampleRate"_s, info.audioProperties()->sampleRate()},
            {u"channels"_s, info.audioProperties()->channels()}
        };
    }

    extractFlacImage(info.tag()->pictureList(), media);
    
    return media;
}

static inline media_t extractFromOpus(const QString &file)
{
    TagLib::Ogg::Opus::File info(file.toUtf8().constData());

    media_t media { .path = QUrl::fromLocalFile(file) };
    extractXiphComment(info.tag(), media);
    
    if (info.audioProperties()) {
        media.length = info.audioProperties()->lengthInMilliseconds();
        media.audioProperties = {
            {u"bitrate"_s, info.audioProperties()->bitrate()},
            {u"sampleRate"_s, info.audioProperties()->sampleRate()},
            {u"channels"_s, info.audioProperties()->channels()},
            {u"inputSampleRate"_s, info.audioProperties()->inputSampleRate()},
            {u"opusVersion"_s, info.audioProperties()->opusVersion()}
        };
    }

    extractFlacImage(info.tag()->pictureList(), media);
    
    return media;
}

static inline media_t extractFromFlac(const QString &file)
{
    media_t media { .path = QUrl::fromLocalFile(file) };
    TagLib::FLAC::File info(file.toUtf8().constData());
    
    if (info.hasXiphComment()) {
        extractXiphComment(info.xiphComment(), media);
    }
    else if(info.hasID3v2Tag()) {
        extractID3v2(info.ID3v2Tag(), media);
    }

    if (info.audioProperties()) {
        media.length = info.audioProperties()->lengthInMilliseconds();
        media.audioProperties = {
            {u"bitrate"_s, info.audioProperties()->bitrate()},
            {u"sampleRate"_s, info.audioProperties()->sampleRate()},
            {u"channels"_s, info.audioProperties()->channels()},
            {u"bitsPerSample"_s, info.audioProperties()->bitsPerSample()}
        };
    }

    extractFlacImage(info.pictureList(), media);
    
    return media;
}

static inline QMimeType resolveMimeType(const QUrl &file)
{
    QMimeDatabase db;
    auto typesByExtension = db.mimeTypesForFileName(file.toLocalFile());
    auto typeByContent = db.mimeTypeForFile(file.toLocalFile(), QMimeDatabase::MatchContent);

    if (typesByExtension.contains(typeByContent)) {
        return typeByContent;
    }

    for (const auto &mime : typesByExtension) {
        if (mime.inherits(typeByContent.name())) {
            return mime;
        }
    }

    return typeByContent;
}


static media_t extract(const QUrl &file)
{
    const auto mime = resolveMimeType(file);
    const auto typeName = mime.name();

    if (typeName == u"audio/flac"_s) {
        return extractFromFlac(file.toLocalFile());
    }

    if (typeName == u"audio/mp4"_s) {
        return extractFromMp4(file.toLocalFile());
    }

    if (typeName == u"audio/mpeg"_s) {
        return extractFromMpeg(file.toLocalFile());
    }

    if (typeName == u"audio/vorbis"_s || typeName == u"audio/x-vorbis+ogg"_s
        || typeName == u"audio/ogg"_s)
    {
        return extractFromOgg(file.toLocalFile());
    }
    
    if (typeName == u"audio/opus"_s || typeName == u"audio/x-opus+ogg"_s) {
        return extractFromOpus(file.toLocalFile());
    }
    
    return {
        .path = file,
        .title = file.fileName()
    };
}

/*
 *
 * TagReader class
 *
 */

TagReader::TagReader(QObject *parent) : QObject(parent)
{
    QObject::connect(&m_futureWatcher, SIGNAL(progressRangeChanged(int,int)), this, SIGNAL(progressRangeChanged(int,int)));
    QObject::connect(&m_futureWatcher, SIGNAL(progressTextChanged(const QString &)), this, SIGNAL(progressTextChanged(const QString&)));
    QObject::connect(&m_futureWatcher, SIGNAL(progressValueChanged(int)), this, SIGNAL(progressValueChanged(int)));
    QObject::connect(&m_futureWatcher, SIGNAL(finished()), this, SIGNAL(finished()));
    QObject::connect(&m_futureWatcher, SIGNAL(finished()), this, SLOT(onFinished()));
    QObject::connect(&m_futureWatcher, SIGNAL(resultReadyAt(int)), this, SLOT(onResultReadyAt(int)));
}

void TagReader::setFiles(const QList<QUrl> &files)
{
    if (m_futureWatcher.isRunning()) {
        m_queue << files;
        return;
    }
    
    m_futureWatcher.setFuture(QtConcurrent::mapped(files, extract));
}

void TagReader::onResultReadyAt(int index)
{
    emit ready(m_futureWatcher.resultAt(index));
}

MediaList TagReader::results() const
{
    if (m_futureWatcher.isFinished()) {
        return m_futureWatcher.future().results();
    }

    return {};
}

void TagReader::onFinished()
{
    if (m_queue.isEmpty()) {
        return;
    }
    
    m_futureWatcher.setFuture(QtConcurrent::mapped(m_queue, extract));
    m_queue.clear();
}

QFuture<MediaList> TagReader::extractMetaData(const QList<QUrl> &files)
{
    return QtConcurrent::run([files]() {
        MediaList mediaList;
        mediaList.reserve(files.size());

        for (const auto &file : files) {
            mediaList.append(extract(file));
        }

        return mediaList;
    });
}
