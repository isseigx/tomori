#ifndef SONG_H
#define SONG_H

#include <QMetaType>
#include <QObject>
#include <QString>
#include <QUrl>

struct song_t
{
    Q_GADGET
    
    Q_PROPERTY(QUrl path MEMBER path)
    Q_PROPERTY(QString title MEMBER title)
    Q_PROPERTY(QString artist MEMBER artist)
    Q_PROPERTY(QString albumArtist MEMBER albumArtist)
    Q_PROPERTY(QString album MEMBER album)
    Q_PROPERTY(QUrl imagePath MEMBER imagePath)
    Q_PROPERTY(QString lyrics MEMBER lyrics)
    Q_PROPERTY(uint year MEMBER year)
    Q_PROPERTY(uint track MEMBER track)
    Q_PROPERTY(int length MEMBER length)
    Q_PROPERTY(QString genre MEMBER genre)
    Q_PROPERTY(QString comment MEMBER comment)
public:
    QUrl path;
    QString title;
    QString artist;
    QString album;
    QString albumArtist;
    QUrl imagePath;
    QString lyrics;
    uint year;
    uint track;
    int length;
    QString genre;
    QString comment;
};

typedef QList<song_t> SongList;

Q_DECLARE_METATYPE(SongList)

#endif //SONG_H
