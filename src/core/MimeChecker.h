#ifndef MIMECHECKER_H
#define MIMECHECKER_H

#include <QMimeDatabase>
#include <QMimeType>
#include <QString>
#include <QStringList>

namespace MimeChecker {

    static QStringList audioMimeTypes() {
        QStringList types({
            QLatin1String("audio/vorbis"),
            QLatin1String("audio/x-vorbis+ogg"),
            QLatin1String("audio/opus"),
            QLatin1String("audio/x-opus+ogg"),
            QLatin1String("audio/ogg"),
            QLatin1String("audio/flac"),
            QLatin1String("audio/x-flac"),
            QLatin1String("audio/mpeg"),
            QLatin1String("audio/mp4"),
        });
        
        return types;
    }
    
    static QStringList imageMimeTypes() {
        QStringList types({
            QLatin1String("image/jpg"),
            QLatin1String("image/jpeg"),
            QLatin1String("image/png")
        });
        
        return types;
    }
    
    static bool isSupported(const QString &file, const QStringList &types)
    {
        QMimeDatabase db;
        
        const auto mimeType = db.mimeTypeForFile(file);

        for (const auto &type : types) {
            if (mimeType.inherits(type)) {
                return true;
            }
        }
        return false;
    }
    
    static bool isAudioFileSupported(const QString &file)
    {
        return isSupported(file, audioMimeTypes());
    }
    
    static bool isImageFileSupported(const QString &file)
    {
        return isSupported(file, imageMimeTypes());
    }
}

#endif //MIMECHECKER_H
