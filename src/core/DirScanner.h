#ifndef DIRSCANNER_H
#define DIRSCANNER_H

#include <QFutureWatcher>
#include <QObject>
#include <QUrl>

class DirScan
{

public:
    DirScan();
    static QString getImageForFile(const QString &);
    QList<QUrl> scan(const QString &path);

};

class DirScanner : public QObject
{
   Q_OBJECT

public:
    explicit DirScanner(QObject *parent = nullptr);
    ~DirScanner();

    static QFuture<QList<QUrl>> scan(const QUrl &path);

public slots:
    void setPath(const QString &path);

signals:
    void progressRangeChanged(int minimum, int maximum);
    void progressTextChanged(const QString &progressText);
    void progressValueChanged(int progressValue);
    void finished(QList<QUrl> result);
    
private:
    Q_SLOT void onFutureFinished();
    
    QFutureWatcher<QList<QUrl>> m_futureWatcher;
};

#endif //DIRSCANNER_H
