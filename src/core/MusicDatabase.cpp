#include "MusicDatabase.h"

#include <QDebug>
#include <QSqlError>
#include <QSqlQuery>
#include <QStandardPaths>

#define DATABASE_VERSION 4

#define TABLE_SQL "CREATE TABLE IF NOT EXISTS library(title TEXT, artist TEXT, album TEXT, "\
                  "path TEXT NOT NULL UNIQUE, albumArtist TEXT, image TEXT, lyrics TEXT, year INT, "\
                  "track INT, length INT, genre TEXT, comment TEXT)"

#define ALBUMS_VIEW "CREATE VIEW IF NOT EXISTS albums_view AS"\
                    "SELECT artist,album,albumArtist,year,count(title) AS tracks,"\
                    "sum(library.length) AS totalTime FROM library GROUP BY artist ORDER BY album;"

using namespace Qt::Literals::StringLiterals;

MusicDatabase::MusicDatabase()
{
    init(mDefaultConnectionName);
}

void MusicDatabase::init(const QString &connectionName)
{
    mCurrentConnectionName = connectionName;

    QString db_name;
    const auto db_path = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation).constFirst();
    
    if (mTemporaryMode) {
        db_name = QLatin1String(":memory:");
    } else {
        db_name = u"%1/musicdb.sqlite3"_s.arg(db_path);
    }
    
    QSqlDatabase db = QSqlDatabase::addDatabase(u"QSQLITE"_s, connectionName);
    db.setDatabaseName(db_name);

    if (!db.open()) {
        emit error(Enums::Connection, db.lastError().text());
        return;
    }
    else {
        emit finished(Enums::Connection);
    }

    if (updateDatabase()) {
        return;
    }

    QSqlQuery query(db);

    if (!query.exec(TABLE_SQL)) {
        emit error(Enums::Create, query.lastError().text());
        return;
    }
    else {
        emit finished(Enums::Create);
    }
}

void MusicDatabase::addConnection()
{
    const QString name = QStringLiteral("%1_%2").arg(mDefaultConnectionName, 
                                                         QString::number(QSqlDatabase::connectionNames().size()));
    init(name);
}

void MusicDatabase::removeConnection()
{
    QSqlDatabase::removeDatabase(mCurrentConnectionName);
    
    const QStringList names = QSqlDatabase::connectionNames();
    
    if (QSqlDatabase::contains(mDefaultConnectionName) || names.isEmpty()) {
        mCurrentConnectionName = mDefaultConnectionName;
    }
    else {
        mCurrentConnectionName = names.first();
    }
}

void MusicDatabase::addSong(const media_t &s)
{
    addSongs({s});
}

void MusicDatabase::addSongs(const MediaList &songs)
{
    QSqlQuery query(QSqlDatabase::database(mCurrentConnectionName));

    QString sql(u"INSERT INTO library(title,artist,album,path,albumArtist,image,lyrics,year,track,length,genre,comment)"
                " VALUES(?,?,?,?,?,?,?,?,?,?,?,?) ON CONFLICT(path) DO"
                " UPDATE SET title = excluded.title, artist=excluded.artist, album=excluded.album, image=excluded.image,"
                " albumArtist=excluded.albumArtist, lyrics=excluded.lyrics, year=excluded.year, track=excluded.track,"
                " length=excluded.length, genre=excluded.genre, comment=excluded.comment, path=excluded.path WHERE"
                " library.path = excluded.path"_s);

    if (QSqlDatabase::database(mCurrentConnectionName).transaction()) {
        for (const media_t &song : songs) {
            query.prepare(sql);

            query.addBindValue(song.title);
            query.addBindValue(song.artist);
            query.addBindValue(song.album);
            query.addBindValue(song.path.toString());
            query.addBindValue(song.albumArtist);
            query.addBindValue(song.image.toString());
            query.addBindValue(song.lyrics);
            query.addBindValue(song.year);
            query.addBindValue(song.track);
            query.addBindValue(song.length);
            query.addBindValue(song.genre);
            query.addBindValue(song.comment);

            query.exec();
        }

        if (!QSqlDatabase::database(mCurrentConnectionName).commit()) {
            emit error(Enums::Insert, QSqlDatabase::database(mCurrentConnectionName).lastError().text());
        }
        else {
            emit finished(Enums::Insert, songs.count());
        }
    }
    else {
        emit error(Enums::Insert, QSqlDatabase::database(mCurrentConnectionName).lastError().text());
    }
}

void MusicDatabase::removeSong(const QUrl &s)
{
    removeSongs({s});
}

void MusicDatabase::removeSongs(const QList<QUrl> &songs)
{
    QSqlQuery query(QSqlDatabase::database(mCurrentConnectionName));

    QString sql(u"DELETE FROM library WHERE path = (?)"_s);

    if (QSqlDatabase::database(mCurrentConnectionName).transaction()) {
        for (const QUrl &s : songs) {
            query.prepare(sql);
            query.addBindValue(s.toString());
            query.exec();
        }

        if (!QSqlDatabase::database(mCurrentConnectionName).commit()) {
            emit error(Enums::Delete, QSqlDatabase::database(mCurrentConnectionName).lastError().text());
        }
        else {
            emit finished(Enums::Delete, songs.count());
        }
    }
    else {
        emit error(Enums::Delete, QSqlDatabase::database(mCurrentConnectionName).lastError().text());
    }
}

void MusicDatabase::updateSong(const media_t &s)
{
    if (getSongs(u"path"_s, s.path.toString()).size() == 0) {
        return addSong(s);
    }
    
    QSqlQuery query(QSqlDatabase::database(mCurrentConnectionName));
    
    query.prepare(u"UPDATE library SET title=?,artist=?,album=?,path=?,"
                                "albumArtist=?,image=?,lyrics=?,year=?,track=?,length=?,"
                                "genre=?,comment=? WHERE path=(?)"_s);
    
    query.addBindValue(s.title);
    query.addBindValue(s.artist);
    query.addBindValue(s.album);
    query.addBindValue(s.path.toString());
    query.addBindValue(s.albumArtist);
    query.addBindValue(s.image.toString());
    query.addBindValue(s.lyrics);
    query.addBindValue(s.year);
    query.addBindValue(s.track);
    query.addBindValue(s.length);
    query.addBindValue(s.genre);
    query.addBindValue(s.comment);
    
    query.addBindValue(s.path.toString());
       
    if (!query.exec()) {
        emit error(Enums::Update, query.lastError().text());
    }
    else {
        emit finished(Enums::Update, 1);
    }
}

void MusicDatabase::updateSongs(const MediaList &songs)
{
    QSqlQuery query(QSqlDatabase::database(mCurrentConnectionName));

    QString sql(u"UPDATE library SET title=?,artist=?,album=?,path=?,"
                "albumArtist=?,image=?,lyrics=?,year=?,track=?,length=?,"
                "genre=?,comment=? WHERE path=(?)"_s);

    if(QSqlDatabase::database().transaction()) {
        for (const media_t &s : songs) {
            query.prepare(sql);

            query.addBindValue(s.title);
            query.addBindValue(s.artist);
            query.addBindValue(s.album);
            query.addBindValue(s.path.toString());
            query.addBindValue(s.albumArtist);
            query.addBindValue(s.image.toString());
            query.addBindValue(s.lyrics);
            query.addBindValue(s.year);
            query.addBindValue(s.track);
            query.addBindValue(s.length);
            query.addBindValue(s.genre);
            query.addBindValue(s.comment);
            query.addBindValue(s.path.toString());

            query.exec();
        }

        if (!QSqlDatabase::database().commit()) {
            emit error(Enums::Update, query.lastError().text());
        }
        else {
            emit finished(Enums::Update, songs.count());
        }
    }
    else {
        emit error(Enums::Update, QSqlDatabase::database(mCurrentConnectionName).lastError().text());
    }
}

void MusicDatabase::deleteAll()
{
    QSqlQuery query(QSqlDatabase::database(mCurrentConnectionName));

    if (!query.exec(u"DELETE FROM library"_s)) {
        emit error(Enums::Delete, query.lastError().text());
    }
    else {
        emit finished(Enums::Delete);
    }
}

MediaList MusicDatabase::getAllSongs()
{
    MediaList songs;
    QSqlQuery query(QSqlDatabase::database(mCurrentConnectionName));

    QString sql(u"SELECT title,artist,album,path,albumArtist,image,"
                "lyrics,year,track,length,genre,comment FROM library"_s);

    if (!query.exec(sql)) {
        emit error(Enums::Select, query.lastError().text());
        return songs;
    }

    while (query.next()) {
        songs.append({
            .path = query.value(3).toUrl(),
            .title = query.value(0).toString(),
            .artist = query.value(1).toString(),
            .album = query.value(2).toString(),
            .albumArtist = query.value(4).toString(),
            .image = query.value(5).toUrl(),
            .lyrics = query.value(6).toString(),
            .year = query.value(7).toUInt(),
            .track = query.value(8).toUInt(),
            .length = query.value(9).toInt(),
            .genre = query.value(10).toString(),
            .comment = query.value(11).toString()
        });
    }

    return songs;
}

MediaList MusicDatabase::getSongs(const QString &field, const QVariant &condition)
{
    MediaList songs;
    QSqlQuery query(QSqlDatabase::database(mCurrentConnectionName));

    query.prepare(u"SELECT title,artist,album,path,albumArtist,image,lyrics,year,track,"
                                "length,genre,comment FROM library WHERE %1 = ? ORDER BY track ASC"_s
                                .arg(field));
    
    query.addBindValue(condition);

    if (!query.exec()) {
        emit error(Enums::Select, query.lastError().text());
        return songs;
    }

    while (query.next()) {
        songs.append({
            .path = query.value(3).toUrl(),
            .title = query.value(0).toString(),
            .artist = query.value(1).toString(),
            .album = query.value(2).toString(),
            .albumArtist = query.value(4).toString(),
            .image = query.value(5).toUrl(),
            .lyrics = query.value(6).toString(),
            .year = query.value(7).toUInt(),
            .track = query.value(8).toUInt(),
            .length = query.value(9).toInt(),
            .genre = query.value(10).toString(),
            .comment = query.value(11).toString()
        });
    }

    return songs;
}

MediaList MusicDatabase::getAlbums()
{
    MediaList albums;
    
    QSqlQuery query(QSqlDatabase::database(mCurrentConnectionName));
    
    QString sql(u"SELECT COUNT(album),artist,album,albumArtist,image,year "
                              "FROM library GROUP BY album"_s);

    if (!query.exec(sql)) {
        emit error(Enums::Select, query.lastError().text());
        return albums;
    }

    while (query.next()) {
        albums.append({
            .path = QUrl(),
            .artist = query.value(1).toString(),
            .album = query.value(2).toString(),
            .albumArtist = query.value(3).toString(),
            .image = query.value(4).toUrl(),
            .year = query.value(5).toUInt()
        });
    }

    return albums;
}

MediaList MusicDatabase::getAlbums(const QString &artist)
{
    MediaList albums;
    QSqlQuery query(QSqlDatabase::database(mCurrentConnectionName));

    query.prepare(u"SELECT COUNT(album),artist,album,albumArtist,image,year,track "
                                "FROM library WHERE artist = ? GROUP BY album"_s);
    query.addBindValue(artist);

    if (!query.exec()) {
        emit error(Enums::Select, query.lastError().text());
        return albums;
    }

    while (query.next()) {
        albums.append({
            .artist = query.value(1).toString(),
            .album = query.value(2).toString(),
            .albumArtist = query.value(3).toString(),
            .image = query.value(4).toUrl(),
            .year = query.value(5).toUInt(),
            .track = query.value(6).toUInt()
        });
    }

    return albums;
}

MediaList MusicDatabase::getArtists()
{
    MediaList artists;
    QSqlQuery query(QSqlDatabase::database(mCurrentConnectionName));
    
    QString sql(u"SELECT COUNT(rowid), artist, image from library GROUP BY artist"_s);

    if (!query.exec(sql)) {
        emit error(Enums::Select, query.lastError().text());
        return artists;
    }

    while (query.next()) {
        artists.append({
            .artist = query.value(1).toString(),
            .image = query.value(2).toString()
        });
    }

    return artists;
}

QList<QUrl> MusicDatabase::getFiles()
{
    QList<QUrl> files;
    QSqlQuery query(QSqlDatabase::database(mCurrentConnectionName));
    
    if (!query.exec(u"SELECT path FROM library"_s)) {
        emit error(Enums::Select, query.lastError().text());
        return files;
    }
    
    while (query.next()) {
        files << query.value(0).toUrl();
    }
    
    return files;
}

void MusicDatabase::setTemporaryMode(bool mode)
{
    if (mode != mTemporaryMode) {
        mTemporaryMode = mode;
        removeConnection();
        addConnection();
    }
}

bool MusicDatabase::libraryExists()
{
    QSqlQuery query(QSqlDatabase::database(mCurrentConnectionName));
    
    QString sql(u"SELECT COUNT(*) FROM sqlite_master WHERE "
                              "type='table' AND name='library'"_s);
        
    if (!query.exec(sql)) {
        emit error(Enums::Select, query.lastError().text());
        return false;
    }

    query.next();
    const int result = query.value(0).toInt();

    return result == 1;
}

int MusicDatabase::version()
{
    QSqlQuery query(QSqlDatabase::database(mCurrentConnectionName));
    
    if (!query.exec(u"PRAGMA user_version"_s)) {
        emit error(Enums::Select, query.lastError().text());
        return -1;
    }

    query.next();
    return query.value(0).toInt();
}

bool MusicDatabase::updateDatabase()
{
    if (DATABASE_VERSION == version()) {
        return false;
    }

    QSqlQuery query(QSqlDatabase::database(mCurrentConnectionName));

    query.exec(u"DROP TABLE IF EXISTS library"_s);
    query.exec(u"PRAGMA user_version = %1"_s.arg(DATABASE_VERSION));
    query.exec(TABLE_SQL);

    qDebug("[Ayase Database]: updating database version to: %d", DATABASE_VERSION);

    return true;
}
