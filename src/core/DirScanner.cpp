#include "DirScanner.h"

#include <QDir>
#include <QDirIterator>
#include <QFileInfo>
#include <QDebug>
#include <QtConcurrent>

#include "MimeChecker.h"

static inline QList<QUrl> scanDirectory(const QString &path)
{
    QList<QUrl> files;
    const QStringList mimeTypes(MimeChecker::audioMimeTypes());

    QDir dir(path);
    const auto infoList = dir.entryInfoList(QDir::Dirs | QDir::Files | QDir::NoSymLinks
                                            | QDir::NoDotAndDotDot, QDir::DirsFirst | QDir::Name);

    for (const auto &info : infoList) {
        const auto path = info.filePath();
        if (info.isDir()) {
            files << scanDirectory(path);
        }
        else {
            if (MimeChecker::isSupported(path, mimeTypes)) {
                files << QUrl::fromLocalFile(path);
            }
        }
    }
    return files;
}

DirScan::DirScan()
{
}

QList<QUrl> DirScan::scan(const QString &path)
{
    return scanDirectory(path);
}

QString DirScan::getImageForFile(const QString &file)
{
    const QStringList mimeTypes(MimeChecker::imageMimeTypes());

    QFileInfo info(file);
    QDir dir = info.dir();
    
    const auto infolist = dir.entryInfoList(QStringList() << "cover.*" << "folder.*",
        QDir::Files, QDir::Type);
    
    for (const QFileInfo &fi : infolist) {
        if(MimeChecker::isSupported(fi.filePath(), mimeTypes)) {
            return fi.filePath();
        }
    }
    
    return QString();
}

/*
*
*    DIRSCANNER
*
*/

DirScanner::DirScanner(QObject *parent) : QObject(parent)
{
    QObject::connect(&m_futureWatcher, SIGNAL(progressRangeChanged(int,int)), this, SIGNAL(progressRangeChanged(int,int)));
    QObject::connect(&m_futureWatcher, SIGNAL(progressTextChanged(const QString &)), this, SIGNAL(progressTextChanged(const QString&)));
    QObject::connect(&m_futureWatcher, SIGNAL(progressValueChanged(int)), this, SIGNAL(progressValueChanged(int)));
    QObject::connect(&m_futureWatcher, SIGNAL(finished()), this, SLOT(onFutureFinished()));
}

DirScanner::~DirScanner()
{
}

void DirScanner::setPath(const QString &path)
{
    m_futureWatcher.setFuture(QtConcurrent::run(scanDirectory, path));
}

void DirScanner::onFutureFinished()
{
    emit finished(m_futureWatcher.result());
}

QFuture<QList<QUrl>> DirScanner::scan(const QUrl &path)
{
    return QtConcurrent::run(scanDirectory, path.toLocalFile());
}
