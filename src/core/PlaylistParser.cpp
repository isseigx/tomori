#include "PlaylistParser.h"

#include <QFile>
#include <QStandardPaths>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>

PlaylistParser::PlaylistParser(QObject *parent) : QObject(parent)
{
}

PlaylistParser::~PlaylistParser()
{
}

void PlaylistParser::savePlaylist(const MediaList &mediaList)
{
    QString file = QStringLiteral("%1/%2").arg(
        QStandardPaths::standardLocations(QStandardPaths::GenericDataLocation).constFirst(),
        QStringLiteral("ayase_playlist.xspf"));

    if (mediaList.isEmpty()) {
        QFile::remove(file);
        return;
    }
    
    savePlaylist(file, mediaList);
}

void PlaylistParser::savePlaylist(const QString &file, const MediaList &mediaList)
{
    if (mediaList.isEmpty()) {
        return;
    }
    
    QFile f(file);
    
    if (!f.open(QIODevice::WriteOnly)) {
        return;
    }

    QXmlStreamWriter writer(&f);
    writer.setAutoFormatting(true);
    writer.writeStartDocument();
    writer.writeStartElement(QStringLiteral("playlist"));
    writer.writeAttribute(QStringLiteral("version"), QStringLiteral("1"));
    writer.writeDefaultNamespace(QStringLiteral("http://xspf.org/ns/0/"));
    
    writer.writeStartElement(QStringLiteral("trackList"));
    
    for (auto&& media : mediaList) {
        if(!QFile::exists(media.path.toLocalFile()))
            continue;
        
        writer.writeStartElement(QStringLiteral("track"));
        writer.writeTextElement(QStringLiteral("location"), media.path.toString());
        if (!media.title.isEmpty()) {
            writer.writeTextElement(QStringLiteral("title"), media.title);
        }
        if (!media.artist.isEmpty()) {
            writer.writeTextElement(QStringLiteral("creator"), media.artist);
        }
        if (!media.album.isEmpty()) {
            writer.writeTextElement(QStringLiteral("album"), media.album);
        }
        if (!media.image.isEmpty()) {
            writer.writeTextElement(QStringLiteral("image"), media.image.toString());
        }
        writer.writeTextElement(QStringLiteral("duration"), QString::number(media.length));
        
        writer.writeEndElement();
    }
    writer.writeEndDocument();
}

MediaList PlaylistParser::parse() const
{
    const QString file = QStringLiteral("%1/%2").arg(
        QStandardPaths::standardLocations(QStandardPaths::GenericDataLocation).constFirst(),
        QStringLiteral("ayase_playlist.xspf"));

    return parse(file);
}

MediaList PlaylistParser::parse(const QString &file) const
{
    QFile f(file);
    
    if (!f.open(QIODevice::ReadOnly)) {
        return {};
    }
    
    QString title, artist, album, path, image, length;

    MediaList mediaList;
    QXmlStreamReader reader(&f);
    
    while(!reader.atEnd()) {
        QXmlStreamReader::TokenType type = reader.readNext();
        
        if (type == QXmlStreamReader::StartElement) {
        
            auto name = reader.name();
            if(name == QLatin1String("location")) {
                path = reader.readElementText();
            } else if(name == QLatin1String("title")) {
                title = reader.readElementText();
            } else if(name == QLatin1String("creator")) {
                artist = reader.readElementText();
            } else if(name == QLatin1String("album")) {
                album = reader.readElementText();
            } else if (name == QLatin1String("image")) {
                image = reader.readElementText();
            } else if (name == QLatin1String("duration")) {
                length = reader.readElementText();
            }
        
        }
        else if (type == QXmlStreamReader::EndElement) {
            QUrl location(path);

            if(reader.name() == QLatin1String("track")) {
                if (QFile::exists(location.toLocalFile())) {
                    mediaList.append({
                        .path = location,
                        .title = title,
                        .artist = artist,
                        .album = album,
                        .image = QUrl(image),
                        .length = length.toInt()
                    });
                }
                path.clear();
                title.clear();
                artist.clear();
                album.clear();
                image.clear();
                length.clear();
            }
        }
    }
    
    return mediaList;
}
