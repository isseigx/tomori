#ifndef LIBRARYLISTMODEL_H
#define LIBRARYLISTMODEL_H

#include <QAbstractListModel>
#include <QtQml/qqmlregistration.h>

#include "Media.h"

class LibraryListModel : public QAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT

    Q_PROPERTY(int count READ count NOTIFY countChanged)
    Q_PROPERTY(MediaList mediaList READ mediaList WRITE setMediaList NOTIFY mediaListChanged)


public:
    explicit LibraryListModel(QObject *parent = nullptr);
    ~LibraryListModel();

    QVariant data(const QModelIndex &, int = Qt::DisplayRole) const override;

    int rowCount(const QModelIndex &) const override;

    Q_INVOKABLE bool removeRows(int, int, const QModelIndex & = QModelIndex()) override;

    Q_INVOKABLE bool moveRows(const QModelIndex &, int, int, const QModelIndex &, int) override;

    QHash<int, QByteArray> roleNames() const override;

    void setMediaList(const MediaList &);

    Q_INVOKABLE media_t mediaAt(int) const;

    MediaList mediaList() const { return m_mediaList; }

    Q_INVOKABLE void add(const MediaList &);

    int count() const;

public slots:
    void append(const MediaList &);

signals:
    void mediaListChanged();
    void countChanged(int);

private:
    MediaList m_mediaList;
};

#endif //LIBRARYLISTMODEL_H
