#ifndef PLAYLISTPARSER_H
#define PLAYLISTPARSER_H

#include <QObject>
#include <QList>

#include "Media.h"

class PlaylistParser : public QObject
{
    Q_OBJECT
    
public:
    PlaylistParser(QObject *parent = nullptr);
    ~PlaylistParser();
    
    void savePlaylist(const QString &, const MediaList &);
    void savePlaylist(const MediaList &);
    
    MediaList parse() const;
    MediaList parse(const QString &) const;
};

#endif //PLAYLISTPARSER_H
