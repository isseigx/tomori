#ifndef MUSICDATABASE_H
#define MUSICDATABASE_H

#include <QObject>
#include <QSqlDatabase>

#include "Enums.h"
#include "Media.h"

class MusicDatabase : public QObject
{
    Q_OBJECT

public:
    ~MusicDatabase() = default;

    static MusicDatabase& instance()
    {
        static MusicDatabase instance_;
        return instance_;
    }

    void addSong(const media_t &);
    void addSongs(const MediaList &);
    void removeSong(const QUrl &);
    void removeSongs(const QList<QUrl> &);
    void updateSong(const media_t &);
    void updateSongs(const MediaList &);
    void deleteAll();

    MediaList getAllSongs();
    MediaList getSongs(const QString &, const QVariant &);
    MediaList getAlbums();
    MediaList getAlbums(const QString &);
    MediaList getArtists();
    QList<QUrl> getFiles();
 
    void setTemporaryMode(bool mode);
    
    int version();

signals:
    void error(Enums::QueryType type, const QString &message);
    void finished(Enums::QueryType type, int rows = 0);

private:
    MusicDatabase();
    
    bool libraryExists();
    
    void addConnection();
    void init(const QString &);
    void removeConnection();
    bool updateDatabase();

    const QString mDefaultConnectionName = QStringLiteral("ayase_sql_connection");
    QString mCurrentConnectionName;
    bool mTemporaryMode;
};

#endif //MUSICDATABASE
