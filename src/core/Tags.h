#ifndef TAGS_H
#define TAGS_H

#include <QObject>
#include <QFutureWatcher>

#include "Media.h"

class TagReader : public QObject
{
    Q_OBJECT
    
public:
    TagReader(QObject *parent = nullptr);

    static QFuture<MediaList> extractMetaData(const QList<QUrl> &files);
    
    void setFiles(const QList<QUrl> &);
    MediaList results() const;
    
signals:
    void ready(const media_t &media);
    void progressRangeChanged(int minimum, int maximum);
    void progressTextChanged(const QString &progressText);
    void progressValueChanged(int progressValue);
    void finished();

private slots:
    void onResultReadyAt(int);
    void onFinished();
    
private:
    QFutureWatcher<media_t> m_futureWatcher;
    QList<QUrl> m_queue;
};

#endif //TAGS_H
