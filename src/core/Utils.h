#ifndef UTILS_H
#define UTILS_H

#include <QObject>
#include <QQmlEngine>
#include <QUrl>

class Utils : public QObject
{
    Q_OBJECT
    QML_ELEMENT
    QML_SINGLETON
    Q_PROPERTY(QUrl currentDir READ currentDir WRITE setCurrentDir NOTIFY currentDirChanged)
    Q_PROPERTY(QString musicDir READ musicDir CONSTANT)

public:
    static Utils &instance()
    {
        static Utils _instance;
        return _instance;
    }

    static Utils *create(QQmlEngine *engine, QJSEngine *)
    {
        engine->setObjectOwnership(&instance(), QQmlEngine::CppOwnership);
        return &instance();
    }

    ~Utils();

    QString cacheDir() const;
    QString musicDir() const;

    const QUrl& currentDir() const { return m_currentdir; }
    void setCurrentDir(const QUrl &);

    Q_INVOKABLE QString timeToString(qint64 msecs) const;

    // check if given files are supported by player
    Q_INVOKABLE QList<QUrl> checkFiles(const QList<QUrl> &files) const;

signals:
    void currentDirChanged(const QUrl &directory);

private:
    explicit Utils(QObject *parent = nullptr);
    QUrl m_currentdir;
};

#endif //UTILS_H
