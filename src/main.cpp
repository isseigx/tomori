#include <QApplication>
#include <QIcon>
#include <QCommandLineParser>
#include <QQmlApplicationEngine>
#include <QQuickStyle>

#include <QCoroQml>

#include "core/Media.h"
#include "dbus/MPRIS.h"

//#include "ui/ArtistImageProvider.h"

int main(int argc, char *argv[])
{
    QIcon::setFallbackSearchPaths(QIcon::fallbackSearchPaths() << QStringLiteral(":icons"));

    QApplication app(argc, argv);
    app.setApplicationName(QStringLiteral("Ayase"));
    app.setApplicationVersion(AYASE_VERSION);
    app.setDesktopFileName(QStringLiteral("dev.onigiri.ayase"));
    app.setWindowIcon(QIcon::fromTheme(QStringLiteral("ayase")));
    app.setOrganizationName(QStringLiteral("dev.onigiri"));

    QQuickStyle::setStyle("org.kde.desktop");

    qRegisterMetaType<media_t>();
    qRegisterMetaType<MediaList>();

    QCoro::Qml::registerTypes();

    QCommandLineParser parser;
    parser.setApplicationDescription(QCoreApplication::translate("main", "Simple music player"));
    parser.addHelpOption();
    parser.addVersionOption();

    parser.addPositionalArgument(QStringLiteral("file"),
        QCoreApplication::translate("main", "File to play"));


    parser.process(app);
    const QStringList args = parser.positionalArguments();

    if (!args.isEmpty()) {
        Mpris::instance().playUri(args.constFirst());
    }

    if (Mpris::instance().registered()) {
        qDebug("Ayase is already running...");
        return 0;
    }

    QQmlApplicationEngine engine;
    //engine.addImageProvider(QStringLiteral("artist"), new ArtistImageProvider);

    engine.loadFromModule("dev.onigiri.ayase", "Main");

    return app.exec();
}
