#ifndef POWERMANAGER_H
#define POWERMANAGER_H

class QTimer;

class PowerManager
{
    
public:
    explicit PowerManager();
    ~PowerManager();
    void startTimer();
    
private:
    QTimer *activityTimer;
};

#endif //POWERMANAGER_H
