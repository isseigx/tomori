#include "Settings.h"
#include <QSettings>

Settings::Settings()
{
}

void Settings::readPlayerSettings()
{
    QSettings s;
    s.beginGroup(QStringLiteral("Player"));
    outputdevice = s.value(QStringLiteral("OutputDevice"), -1).toInt();
    tagsonplayback = s.value(QStringLiteral("TagsOnPlayback"), true).toBool();
    s.endGroup();
}

void Settings::readMiniUiSettings()
{
    QSettings s;
    s.beginGroup(QStringLiteral("MiniUi"));
    miniuipos = s.value(QStringLiteral("Position"), QPoint(10, 10)).toPoint();
    s.endGroup();
}

void Settings::writePlayerSettings()
{
    QSettings s;
    s.beginGroup(QStringLiteral("Player"));
    s.setValue(QStringLiteral("OutputDevice"), outputdevice);
    s.setValue(QStringLiteral("TagsOnPlayback"), tagsonplayback);
    s.endGroup();
}

void Settings::writeMiniUiSettings()
{
    QSettings s;
    s.beginGroup(QStringLiteral("MiniUi"));
    s.setValue(QStringLiteral("Position"), miniuipos);
    s.endGroup();
}
