#ifndef SETTINGS_H
#define SETTINGS_H

#include <QPoint>

class Settings
{
public:
    Settings();
    
    void readPlayerSettings();
    void readMiniUiSettings();
    
    void writePlayerSettings();
    void writeMiniUiSettings();
    
    void setAudioOutputDevice(int index) { outputdevice = index; }
    void setMiniUiPosition(const QPoint &p) { miniuipos = p; }
    void setReadTagOnPlayback(bool read) { tagsonplayback = read; }
    
    int audioOutputDevice() { return outputdevice; }
    QPoint miniUiPosition() { return miniuipos; }
    bool readTagOnPlayback() { return tagsonplayback; }
    
private:
    int outputdevice;
    QPoint miniuipos;
    bool tagsonplayback;
};

#endif //SETTINGS_H
