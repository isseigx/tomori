#include "PowerManager.h"

#include <QTimer>

#include <KIdleTime>

PowerManager::PowerManager()
{
}

PowerManager::~PowerManager()
{
}

void PowerManager::startTimer()
{
    activityTimer = new QTimer();
    activityTimer->start(60000);

    QObject::connect(activityTimer, &QTimer::timeout, [=]() {
        KIdleTime::instance()->simulateUserActivity();
    });
}
