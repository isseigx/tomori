## Ayase

Minimalistic music player for the linux desktop using [Qt](https://qt.io)/C++ and [libVLC](https://wiki.videolan.org/LibVLC).

[![Ceasefire Now](https://badge.techforpalestine.org/default)](https://techforpalestine.org/learn-more)

### Features

- Read and write XSPF playlists
- MPRIS support
- Read audio tags
- Play FLAC, MP3, M4A, Ogg and Opus files
- Music library with songs, artists and albums views
- File view
- Keep current playlist across sessions
- Lyrics

### Dependencies

- Qt5 Base
- Qt5 Quick Controls 2
- Qt5 Graphical Effects
- KIdleTime (to disable power management)
- Kirigami
- qqc2-desktop-style (follow desktop theme)
- libVLC (playback engine)
- TagLib (files metadata)
- CMake (build system)

### Building / Installing

In order to build Ayase from source, clone this repository and run:

```
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr ..
make
sudo make install
```

### License

GPL v3+

### Screenshots

![Albums](https://i.imgur.com/Ace6L8Z.png)
![Artists](https://i.imgur.com/8CKxXjR.png)
![Songs](https://i.imgur.com/MtHZlPw.png)
![Song details](https://i.imgur.com/qBZESQy.png)
![Album view](https://i.imgur.com/cUDz0Qy.png)
![Lyrics](https://i.imgur.com/Q7NitQ5.png)
![Files](https://i.imgur.com/5EhfA55.png)
![Settings](https://i.imgur.com/hmPJDB4.png)
