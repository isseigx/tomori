### Tomori ToDo List

- [X] Use Label instead of QLabel on mini mode
- [X] Drag elements in playlist to reorder them
- [X] Keep current playlist across sessions
- [X] Music library
- [X] Save current playlist to disc
- [] Open playlist from disc (m3u, pls, ...)
- [X] Minimize to Tray
- [] Application Preferences
- [X] MiniUi keeps previous window position
- [X] Drag folder
- [X] Fix open with (currently tomori doesn't play the file added)
