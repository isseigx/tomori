## 1.7.0
For complete change list, view commit history.

### New
- UI is using Kirigami (again, Qt Widgets in next release? 🤔)
- Add Files page
- Add icons to navigation drawer actions
- Search in Songs, Artists and Albums pages
- Read, write and show playlist duration
- Read year, track and comment fiels from files meta data
- Async meta data extraction for files added to playlist
- Select the default start up page
- Toggle update database on start up
- Many small and not to small UI changes

### Fixes
- Mime types are detected based on file content
- Handle files with mime type audio/ogg

### Code
- Code refactor and clean up thanks to clazy

## 1.6.1
Bug fixes releace
### Fixed
- File dialogs in portrait mode
- Pass invalid cover image to desktop notification
- Register QImage as DBus metatype every time a notification is sent
- Disabled playback controls
- Stop playback before application destruction, this fixes libVLC core dumps

### Code
- Move Playlist view and Playlist menu to component files

## 1.6.0
### Fixed
- TagLib 1.12 deprecation warning when using MP4::ItemListMap
- Playlist model is not reseted when list is randomized
- Previous button icon source
- Add source icon to "more" menu button in Now Playing page
- Append artists or albums do not update views properly

### New
- Now Playing page in portrait mode has new look
- Main window footer contains current playing song metadata
- The amount of columns in Albums grid is dinamically calculated
- "More menu" has all playlist releated actions
- And Many more small UI changes...

## 1.5.0
### New
- List items in Songs page now show cover art, title, artist and album
- Material is the default style
- Window is bigger at start
- When open a file from file managers or the command line, application shows Now Playing page
- Added tool tips to some tool buttons
- The updating process of music library was improved

### Fixed
- It is possible drag and drop files in Now Playing page again
- Set default values to files without tags
- Use ToolButton in tool bar of Settings page

### Code
- C++14 is the minimun required standard
- Remove unused files
- Refactor MusicDatabase and Library classes
- Re-write Player and PlaylistModel classes, improving libvlc events handling
- Player's state and media state enums are available in QML
- Remove dependency of KCoreAddons
- Qt GraphicalEffects is required to handle the Material style
 
## Ayase 1.4.0
### New
- Add pause action in system notification (if notification server support actions)
- Add view with all songs in database
- Remove Kirigami2 components, use only Qt Quick Controls
- Add context menu to elements of many list views
- Cover images in Albums view keep their size across all grid sizes
- Move update and force update library to a Settings page
- Add About tab in Settings page
- Add menu with all views in main window
- Better support for portrait window sizes
- Add fallback icons when icon theme does not have requested name

### Fixes
- Removing Kirigami components improve use of system resources and start up times of user interface
- Disable custom library folder

## Ayase 1.3.0
### Fixes
- Player class handle more libvlc events, this helps to avoid many unnecessary UI updates
- Uninstall target works again
- Remove cmake warnings about GNUInstallDirs module in recent versions
- Removed files appears in Library database or remove the wrong file

### New
- Remove list separators
- Remove album info from album song list view
- Use Item Delegate in Artists list
- Show square with artist name first letter in artist view
- When a artist or album item is clicked the view does not split any more
- Title of the library view now shows the current view mode
- Library view now show all actions in a menu
- When playlist is cleared the playback is stopped

## Ayase 1.2.0
### Fixes
- Change player name on MPRIS service
- Play files from file manager, dbus and command line
- Play action of current playlist item update its icon and tooltip according of player state
- File dialogs are created in runtime
### New
- Add previous and next options in desktop notifications (if supported by server implementation)

## Ayase 1.1.0
### Fixes
- Elide text of playlist items
- Add missing scroll bars
- Code clean up
### New
- Update music library on app start up
- Check for files removed from disk
- Update music meta data (tags) on library
- Show artist name in albums view
- Add force update library option

## Ayase 1.0.0
- [A brand new name](https://love-live.fandom.com/wiki/Eli_Ayase) :3
- New icon
- LibVLC replaces QtAV as playback engine
- New UI using Qt Quick Controls 2/Kirigami2
- An early (and possibly with some bugs) music library implementation
- Mini mode and tray icon were removed
- Remove qmake support
- And many bugs were fixed